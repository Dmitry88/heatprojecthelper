#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtWidgets/QMainWindow>
#include <QAxWidget>
#include <QAxObject.h>
#include <QSqlDatabase>
#include "ui_mainwindow.h"
#include "word_editor.h"

// SQL Test
#include <QSqlDatabase>
#include <QSqlTableModel>
#include <QSqlQuery>
#include <QSqlError>
#include <QTableView>
#include <QListView>
#include <QColumnView>

class mainwindow : public QMainWindow
{
	Q_OBJECT

public:
	mainwindow(QWidget *parent = 0);
	~mainwindow();

	

public slots:
	void on_pushButton_Save_clicked();
	void on_pushButton_Print_clicked();

	void on_menu_settings_clicked();

	void on_lineEdit_HeatingSquare_textChanged(const QString& text);

	void on_comboBox_MeterFlowRate_currentIndexChanged(const QString& text);
	void on_comboBox_Dn_currentIndexChanged(const QString& text);

	void on_dateEdit_Project_Date_dateChanged(const QDate & date);





private:
	QSqlDatabase dataBase;
	WordtEditor documentEditor;

private:
	Ui::mainwindowClass ui;

};

#endif // MAINWINDOW_H
