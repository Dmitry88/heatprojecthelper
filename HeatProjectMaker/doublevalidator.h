#include <QDoubleValidator>
#include <QLineEdit>
#include <QToolTip>
#include <QDebug>

class DoubleValidator : public QDoubleValidator {
public:
	DoubleValidator(double bottom, double top, int decimals, QObject* parent) :
		QDoubleValidator(bottom, top, decimals, parent)
	{
		QDoubleValidator::setLocale(QLocale("Russian"));
		lineEdit = qobject_cast<QLineEdit*>(QDoubleValidator::parent());
	}

	QValidator::State validate(QString& input, int& pos) const
	{
		QValidator::State state = QDoubleValidator::validate(input, pos);

		if (QValidator::Acceptable == state)
		{
			lineEdit->setStyleSheet("background-color: rgb(86, 239, 83);");
			QToolTip::hideText();
		}
		else
		{
			lineEdit->setStyleSheet("background-color: rgb(255, 154, 65);");
			QString b = QString::number(QDoubleValidator::bottom());
			QString t = QString::number(QDoubleValidator::top());
			QToolTip::showText(lineEdit->mapToGlobal(QPoint()),
				QString("range [%1, %2]").arg(b, t),
				lineEdit);
		}

		return state;
	}

private:
	QLineEdit* lineEdit;
};