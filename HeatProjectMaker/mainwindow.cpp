﻿#include "mainwindow.h"

#include <QDebug>
#include <QtPlugin>
#include <QThread.h>
#include <QVector>
#include <QAxWidget>
#include <QAxObject.h>
#include <QMap.h>
#include <QFile.h>
#include <QDir.h>
#include <QMessagebox.h>
#include <QDatetime.h>
#include <QToolTip>
#include <qdatetime.h>
#include "calculations.h"
#include "doublevalidator.h"
#include "settingsdialog.h"




inline QString doubleToString(double value, int prec = 2)
{
	return QString::number(value, 'f', prec);
}



mainwindow::mainwindow(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	ui.dateEdit_Project_Date->setDate(QDate::currentDate());
	

	// Set validators

	//ui->lineEdit_AlOptical->setValidator(new DoubleValidator(10.0, 43.0, 3, ui->lineEdit_AlOptical));

	//ui.lineEdit_HeatingSquare->setValidator(new DoubleValidator(10.0, 43.0, 3, ui.lineEdit_HeatingSquare));
	
	qDebug() << QSqlDatabase::drivers();
	//dataBase = QSqlDatabase::addDatabase("QMYSQL", "patient connection");
	//dataBase.setDatabaseName("data.db");

	//qDebug() << "IS DB OPEN: " << dataBase.open();

	//QSqlQuery query(dataBase);
	//// on foreign key in database
	//query.exec("PRAGMA foreign_keys = ON");
	//qDebug() << "#1 " << query.lastError().text();

	////create patient table
	//query.exec("CREATE TABLE HeatProviderInfo "
	//	"(id INTEGER PRIMARY KEY NOT NULL, "
	//	"organisation TEXT, "
	//	"engineer TEXT, "
	//	"provider TEXT "
	//	"); "
	//	);
	//
	//dataBase.open();

	ui.menu_settings->addAction(QString::fromLocal8Bit("Изменение констант для расчетов"), this, SLOT(on_menu_settings_clicked()));

}

mainwindow::~mainwindow()
{
	
}


void mainwindow::on_pushButton_Print_clicked()
{
	QToolTip::showText(ui.lineEdit_Project_Order_Number->mapToGlobal(QPoint()),
		QString("range [%1, %2]").arg(1, 2),
		ui.lineEdit_Project_Order_Number);
}
void mainwindow::on_menu_settings_clicked()
{
	SettingsDialog d(this);
	d.exec();

}

void mainwindow::on_comboBox_MeterFlowRate_currentIndexChanged(const QString& text)
{
	/*if (QString("0.6") == text || QString("1.5") == text)
		ui.comboBox_Dn->setCurrentIndex(0);
	else if (QString("2.5") == text)
		ui.comboBox_Dn->setCurrentIndex(1);*/
}

void mainwindow::on_comboBox_Dn_currentIndexChanged(const QString& text)
{
	/*if (QString("20") == text)
		ui.comboBox_Dn->setCurrentIndex(1);*/
	/*else
		QMessageBox::warning(this,
							 QString::fromLocal8Bit("ВНИМАНИЕ !!!"),
							 QString::fromLocal8Bit("Выберите правильный расход"));*/

}

void mainwindow::on_lineEdit_HeatingSquare_textChanged(const QString& text)
{


	bool bOk = false;
	double val = text.toDouble(&bOk);

	

	if (!bOk) return;

	if (val <= 69.00)
	{
		ui.comboBox_MeterFlowRate->setCurrentIndex(0);
		ui.comboBox_Dn->setCurrentIndex(0);
	}
	else if (val >= 70.00 && val <= 120.00)
	{
		ui.comboBox_MeterFlowRate->setCurrentIndex(1);
		ui.comboBox_Dn->setCurrentIndex(0);
	}
	else if (val > 120.00)
	{
		ui.comboBox_MeterFlowRate->setCurrentIndex(2);
		ui.comboBox_Dn->setCurrentIndex(1);
	}

}


void mainwindow::on_dateEdit_Project_Date_dateChanged(const QDate & date)
{
	
}

void mainwindow::on_pushButton_Save_clicked()
{
	const QString organisationWhatSignProject = ui.comboBox_NameOfOrganisationWhatSignProject->currentText();
	const QString nameOfMainEngineer = ui.comboBox_NameOfMainEngineer->currentText();
	const QString nameOfHeatProvider = ui.comboBox_NameOfHeatProvider->currentText();

	const QString city = ui.lineEdit_City->text();
	const QString street = ui.lineEdit_Street->text();
	const QString apartment = ui.lineEdit_Apartment->text();

	const QString heatingSquare = ui.lineEdit_HeatingSquare->text();
	const QString commenSquare = ui.lineEdit_CommenSquare->text();

	const QString projectOrderNumber = ui.lineEdit_Project_Order_Number->text();

	const QString apartmentFloor = ui.lineEdit_AparatementFloor->text();
	const QString floorWhereMetersIsMounted = ui.lineEdit_FloorWhereMeterIsMounted->text();
	const QString floorCount = ui.lineEdit_FloorCount->text();

	const QString organisationWhoSignProject = ui.comboBox_NameOfOrganisationWhatSignProject->currentText();
	const QString nameOfSeniorEngineer = ui.comboBox_NameOfMainEngineer->currentText();
	const QString heatEnegyProvider = ui.comboBox_NameOfHeatProvider->currentText();

	const QString flow = ui.comboBox_MeterFlowRate->currentText();
	const QString diameter = ui.comboBox_Dn->currentText();
	const QString pipe = ui.comboBox_Pipe->currentText();

	const QString commenHeatingSquareOfAllFlatsInBuilding = ui.lineEdit_CommenHeating_Square_OfAllFlats->text();
	const QString commenHeatingSquareOfMZK = ui.lineEdit_Commen_Heating_Square_Of_MZK->text();

	
	
	QString path = /*QDir::currentPath()*/ QString("E:/")+ QString("Templates/heat_project_template_v_0_2.docx");

	qDebug() << "PATHHHH         ++++++++++++++++" << QDir::currentPath();
	qDebug() << "############################################## Current dir: " << path;
	
	if (!documentEditor.openDocument(path))
	{
		QMessageBox::warning(this, "Error", "Can't open word document. Please check correctness of file path");
		return;
	}


	QString projectDateAndNumber = 
		QString("%1.%2").arg(projectOrderNumber, 
		ui.dateEdit_Project_Date->date().toString("dd.MM.yyyy"));

	documentEditor.replaceValueInTextBox("#_0_Organization_Name", organisationWhatSignProject);
	documentEditor.replaceValueInText("#_0_Organization_Name", organisationWhatSignProject);
	documentEditor.replaceValueInTextBox("#_1_Main_Engineer_Name", nameOfMainEngineer);


	if (ui.comboBox_NameOfHeatProvider->currentText() == QString::fromLocal8Bit(("ПАТ Київенерго")))
	{
		documentEditor.replaceValueInText("#_41_If_KievEnergo", 
			QString::fromLocal8Bit(" ( за умови погодження з\r\n ПАТ «Київенерго» ∑F_і)"));
	}
	else
	{
		documentEditor.replaceValueInText("#_41_If_KievEnergo",
			QString::fromLocal8Bit(""));
	}
		

	//#_3_Project_Number_And_Date
	documentEditor.replaceValueInText("#_3_Project_Number_And_Date", projectDateAndNumber);
	documentEditor.replaceValueInFooter("#_3_Project_Number_And_Date", QString::fromLocal8Bit("%1.ПЗ").arg(projectDateAndNumber));
	documentEditor.replaceValueInTextBox("#_3_Project_Number_And_Date", QString::fromLocal8Bit("%1.ПЗ").arg(projectDateAndNumber));

	documentEditor.replaceValueInText("#_4_Apatment_Number", apartment);
	
	documentEditor.replaceValueInText("#_5_Adress", QString::fromLocal8Bit("м. %1, вул. %2").arg(city, street));

	QString year = QString::number(QDate::currentDate().year());
	documentEditor.replaceValueInText("#_6_ProjectYear", year);
	documentEditor.replaceValueInFooter("#_6_ProjectYear", year);
	documentEditor.replaceValueInTextBox("#_6_ProjectYear", year);
	

	documentEditor.replaceValueInTextBox("#_40_Town_Street_Apartment", 
		QString("%1 %2, %3 %4, %5 %6").arg(QString::fromLocal8Bit("м."), city, 
									QString::fromLocal8Bit("вул."), street,
									QString::fromLocal8Bit("кв."), apartment)
		);
	//documentEditor.replaceValueInTextBox("#_40_Town_Street_Apartment", apartment);
	//documentEditor.replaceValueInTextBox("#_40_Town_Street_Apartment", apartment);

	documentEditor.replaceValueInText("#_7_Apartment_Number", apartment);
	//documentEditor.replaceValueInFooter("#_7_Apartment_Number", apartment);
	//documentEditor.replaceValueInTextBox("#_7_Apartment_Number", apartment);

	documentEditor.replaceValueInText("#_8_Town", city);
	//documentEditor.replaceValueInFooter("#_8_Town", city);

	documentEditor.replaceValueInText("#_9_Street", street);
	//documentEditor.replaceValueInFooter("#_9_Street", street);

	documentEditor.replaceValueInText("#_10_HeatEnergy_Provider", nameOfHeatProvider);

	documentEditor.replaceValueInText("#_11_Commen_Square", doubleToString(commenSquare.toDouble()));
	documentEditor.replaceValueInText("#_12_Heating_Square", doubleToString(heatingSquare.toDouble()));

	documentEditor.replaceValueInText("#_13_Apartment_Floor", apartmentFloor);
	documentEditor.replaceValueInText("#_14_Count_Of_Floor", floorCount);
	documentEditor.replaceValueInText("#_15_Floor_Where_Meter_IsMounted", floorWhereMetersIsMounted);

	documentEditor.replaceValueInText("#_16_WTAtFlowPipe", doubleToString(Calculations::t1));
	documentEditor.replaceValueInText("#_17_WTAtReturnPipe", doubleToString(Calculations::t2));

	double Qomax = Calculations::Qomax(55.5, heatingSquare.toDouble(), 0.25);
	double Qomin = Calculations::Qomin(Qomax, 20.0, 8.0, -22.0);

	double Qmomax = Calculations::Qmomax(Qomax);
	double Qvomax = Calculations::Qvomax(Qmomax);
	double Qvomin = Calculations::Qvomin(Qmomax);

	double Qmonmax = Calculations::Qmonmax(Qomin);
	double qvonmax = Calculations::qvonmax(Qmonmax);
	double qvonmin = Calculations::qvonmin(Qmonmax);

	documentEditor.replaceValueInText("#_18_Qmax_kWt", doubleToString(Qomax, 4));
	documentEditor.replaceValueInText("#_19_Qmax_GCal", doubleToString(Calculations::kWtToGCal(Qomax), 4));

	documentEditor.replaceValueInText("#_20_Dn", ui.comboBox_Dn->currentText());
	//documentEditor.replaceValueInTextBox("#_20_Dn", ui.comboBox_Dn->currentText());
	//documentEditor.replaceValueInFooter("#_20_Dn", ui.comboBox_Dn->currentText());

	documentEditor.replaceValueInText("#_21_Qn", ui.comboBox_MeterFlowRate->currentText());
	//documentEditor.replaceValueInTextBox("#_21_Qn", ui.comboBox_MeterFlowRate->currentText());
	///documentEditor.replaceValueInText("#_21_Qn", ui.comboBox_MeterFlowRate->currentText());

	if (flow == QString("0.6") && diameter == QString("15"))
	{
		documentEditor.replaceValueInText("#_26_qn_min", "0.024");
		documentEditor.replaceValueInText("#_27_qn_mid", "0.6");
		documentEditor.replaceValueInText("#_28_qn_max", "1.2");
	}
	else if (flow == QString("1.5") /*&& diameter == QString("15")*/)
	{
		documentEditor.replaceValueInText("#_26_qn_min", "0.030");
		documentEditor.replaceValueInText("#_27_qn_mid", "1.5");
		documentEditor.replaceValueInText("#_28_qn_max", "3.0");
	}
	else if (flow == QString("2.5") /*&& diameter == QString("20")*/)
	{
		documentEditor.replaceValueInText("#_26_qn_min", "0.050");
		documentEditor.replaceValueInText("#_27_qn_mid", "2.5");
		documentEditor.replaceValueInText("#_28_qn_max", "5.0");
	}

	documentEditor.replaceValueInText("#_29_Qomin_GCal", doubleToString(Calculations::kWtToGCal(Qomin), 4));

	documentEditor.replaceValueInText("#_30_qmomax", doubleToString(Calculations::kWtToGCal(Qmomax), 4));;
	documentEditor.replaceValueInText("#_31_qv0max", doubleToString(Calculations::kWtToGCal(Qvomax), 4));
	documentEditor.replaceValueInText("#_32_qv0min", doubleToString(Calculations::kWtToGCal(Qvomin), 4));

	documentEditor.replaceValueInText("#_33_qmonmax", doubleToString(Calculations::kWtToGCal(Qmonmax), 4));
	documentEditor.replaceValueInText("#_34_qvonmax", doubleToString(Calculations::kWtToGCal(qvonmax), 4));
	documentEditor.replaceValueInText("#_35_qvonmin", doubleToString(Calculations::kWtToGCal(qvonmin), 4));

	if (pipe == QString::fromLocal8Bit("Подающий"))
	{
		documentEditor.replaceValueInText("#_36_1_PipeWhereMeter_Is_Mounted", QString::fromLocal8Bit("подавальному"));
		documentEditor.replaceValueInText("#_36_2_PipeWhereMeter_Is_Mounted", QString::fromLocal8Bit("подавального"));
		documentEditor.replaceValueInText("#_37_1_Where_Temp_Sensor_Is_Mounted", QString::fromLocal8Bit("зворотній"));
		documentEditor.replaceValueInText("#_37_2_Where_Temp_Sensor_Is_Mounted", QString::fromLocal8Bit("зворотньому"));
	}
	else if (pipe == QString::fromLocal8Bit("Обратный"))
	{
		documentEditor.replaceValueInText("#_36_1_PipeWhereMeter_Is_Mounted", QString::fromLocal8Bit("зворотньому"));
		documentEditor.replaceValueInText("#_36_2_PipeWhereMeter_Is_Mounted", QString::fromLocal8Bit("зворотньому"));
		documentEditor.replaceValueInText("#_37_1_Where_Temp_Sensor_Is_Mounted", QString::fromLocal8Bit("подавальний"));
		documentEditor.replaceValueInText("#_37_2_Where_Temp_Sensor_Is_Mounted", QString::fromLocal8Bit("подавальному"));
	}

	double val = commenHeatingSquareOfAllFlatsInBuilding.toDouble();

	documentEditor.replaceValueInText("#_38_Commen_Heating_Square_Of_All_Flats", doubleToString(val, 4));
	val = commenHeatingSquareOfMZK.toDouble();
	documentEditor.replaceValueInText("#_39_Commen_Heating_Square_MZK", doubleToString(val, 4));

	double heatinSquareValue = heatingSquare.toDouble();
	double commenHeatingSquare = commenHeatingSquareOfAllFlatsInBuilding.toDouble();

	
	documentEditor.replaceValueInText("#_40_MZK_Value", doubleToString(heatinSquareValue / commenHeatingSquare, 4));
}




