#include "mainwindow.h"
#include <QtWidgets/QApplication>
#include <QtPlugin>
#include <QDebug.h>

//#ifndef _DEBUG
Q_IMPORT_PLUGIN(QWindowsIntegrationPlugin)
//#endif

double monthEnergyRate(double middleTemp)
{
	double a = (16.0 - middleTemp) / (16.0 + 0.1);
	double b = a*(30.0 / 176.0);
	return 0.097*b;
}

enum : size_t { HeatingDuration = 7 };

const double middleTempArray[HeatingDuration] =
{
	8.1,
	1.9,
	-2.5,
	-4.7,
	-3.6,
	1.0,
	9.0
};


int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	mainwindow w;
	w.show();
	return a.exec();
}
