/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.6.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDateEdit>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_mainwindowClass
{
public:
    QAction *actionDev;
    QWidget *centralWidget;
    QGridLayout *gridLayout_2;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents;
    QGridLayout *gridLayout_6;
    QGroupBox *groupBox_8;
    QGridLayout *gridLayout_4;
    QLabel *label_24;
    QComboBox *comboBox_NameOfOrganisationWhatSignProject;
    QPushButton *pushButton;
    QLabel *label_25;
    QComboBox *comboBox_NameOfMainEngineer;
    QPushButton *pushButton_2;
    QLabel *label_26;
    QComboBox *comboBox_NameOfHeatProvider;
    QPushButton *pushButton_3;
    QGroupBox *groupBox_2;
    QGridLayout *gridLayout_7;
    QLabel *label_28;
    QDateEdit *dateEdit_Project_Date;
    QLabel *label_29;
    QLineEdit *lineEdit_Project_Order_Number;
    QCheckBox *checkBox;
    QGroupBox *groupBox_10;
    QGridLayout *gridLayout;
    QLabel *label_15;
    QLineEdit *lineEdit_CommenHeating_Square_OfAllFlats;
    QLabel *label_30;
    QLabel *label_27;
    QLineEdit *lineEdit_Commen_Heating_Square_Of_MZK;
    QLabel *label_31;
    QGroupBox *groupBox_9;
    QGridLayout *gridLayout_14;
    QCheckBox *checkBox_5;
    QLineEdit *lineEdit_AparatementFloor;
    QCheckBox *checkBox_6;
    QLineEdit *lineEdit_FloorWhereMeterIsMounted;
    QCheckBox *checkBox_7;
    QLineEdit *lineEdit_FloorCount;
    QGroupBox *groupBox_6;
    QGridLayout *gridLayout_11;
    QLabel *label_17;
    QLineEdit *lineEdit_HeatingSquare;
    QLabel *label_18;
    QLabel *label_19;
    QLineEdit *lineEdit_CommenSquare;
    QLabel *label_20;
    QGroupBox *groupBox_7;
    QGridLayout *gridLayout_3;
    QLabel *label_21;
    QComboBox *comboBox_MeterFlowRate;
    QLabel *label_22;
    QComboBox *comboBox_Dn;
    QLabel *label_23;
    QComboBox *comboBox_Pipe;
    QLabel *label_32;
    QLineEdit *lineEdit_HeatEnergyProvider_3;
    QGroupBox *groupBox_4;
    QGridLayout *gridLayout_5;
    QLabel *label_13;
    QLineEdit *lineEdit_City;
    QLabel *label_14;
    QLineEdit *lineEdit_Street;
    QLabel *label_16;
    QLineEdit *lineEdit_Apartment;
    QGroupBox *groupBox_5;
    QGridLayout *gridLayout_9;
    QRadioButton *radioButton_3;
    QRadioButton *radioButton_4;
    QPushButton *pushButton_Save;
    QPushButton *pushButton_Print;
    QMenuBar *menuBar;
    QMenu *menu;
    QMenu *menu_settings;
    QMenu *menu_3;
    QToolBar *mainToolBar;
    QButtonGroup *buttonGroup;

    void setupUi(QMainWindow *mainwindowClass)
    {
        if (mainwindowClass->objectName().isEmpty())
            mainwindowClass->setObjectName(QStringLiteral("mainwindowClass"));
        mainwindowClass->resize(740, 777);
        mainwindowClass->setStyleSheet(QStringLiteral(""));
        actionDev = new QAction(mainwindowClass);
        actionDev->setObjectName(QStringLiteral("actionDev"));
        centralWidget = new QWidget(mainwindowClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        gridLayout_2 = new QGridLayout(centralWidget);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        scrollArea = new QScrollArea(centralWidget);
        scrollArea->setObjectName(QStringLiteral("scrollArea"));
        scrollArea->setFrameShape(QFrame::StyledPanel);
        scrollArea->setFrameShadow(QFrame::Sunken);
        scrollArea->setLineWidth(0);
        scrollArea->setMidLineWidth(0);
        scrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QStringLiteral("scrollAreaWidgetContents"));
        scrollAreaWidgetContents->setGeometry(QRect(0, 0, 720, 724));
        gridLayout_6 = new QGridLayout(scrollAreaWidgetContents);
        gridLayout_6->setSpacing(6);
        gridLayout_6->setContentsMargins(11, 11, 11, 11);
        gridLayout_6->setObjectName(QStringLiteral("gridLayout_6"));
        groupBox_8 = new QGroupBox(scrollAreaWidgetContents);
        groupBox_8->setObjectName(QStringLiteral("groupBox_8"));
        groupBox_8->setMinimumSize(QSize(381, 114));
        groupBox_8->setMaximumSize(QSize(916, 114));
        QFont font;
        font.setPointSize(10);
        font.setBold(true);
        font.setWeight(75);
        groupBox_8->setFont(font);
        gridLayout_4 = new QGridLayout(groupBox_8);
        gridLayout_4->setSpacing(6);
        gridLayout_4->setContentsMargins(11, 11, 11, 11);
        gridLayout_4->setObjectName(QStringLiteral("gridLayout_4"));
        label_24 = new QLabel(groupBox_8);
        label_24->setObjectName(QStringLiteral("label_24"));
        QFont font1;
        font1.setPointSize(10);
        font1.setBold(false);
        font1.setWeight(50);
        label_24->setFont(font1);

        gridLayout_4->addWidget(label_24, 0, 0, 1, 1);

        comboBox_NameOfOrganisationWhatSignProject = new QComboBox(groupBox_8);
        comboBox_NameOfOrganisationWhatSignProject->setObjectName(QStringLiteral("comboBox_NameOfOrganisationWhatSignProject"));
        comboBox_NameOfOrganisationWhatSignProject->setMinimumSize(QSize(90, 20));
        comboBox_NameOfOrganisationWhatSignProject->setMaximumSize(QSize(180, 20));
        comboBox_NameOfOrganisationWhatSignProject->setFont(font1);
        comboBox_NameOfOrganisationWhatSignProject->setEditable(true);
        comboBox_NameOfOrganisationWhatSignProject->setMaxVisibleItems(40);
        comboBox_NameOfOrganisationWhatSignProject->setDuplicatesEnabled(false);
        comboBox_NameOfOrganisationWhatSignProject->setFrame(true);
        comboBox_NameOfOrganisationWhatSignProject->setModelColumn(0);

        gridLayout_4->addWidget(comboBox_NameOfOrganisationWhatSignProject, 0, 1, 1, 1);

        pushButton = new QPushButton(groupBox_8);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setMinimumSize(QSize(20, 20));
        pushButton->setMaximumSize(QSize(20, 20));

        gridLayout_4->addWidget(pushButton, 0, 2, 1, 1);

        label_25 = new QLabel(groupBox_8);
        label_25->setObjectName(QStringLiteral("label_25"));
        label_25->setFont(font1);

        gridLayout_4->addWidget(label_25, 1, 0, 1, 1);

        comboBox_NameOfMainEngineer = new QComboBox(groupBox_8);
        comboBox_NameOfMainEngineer->setObjectName(QStringLiteral("comboBox_NameOfMainEngineer"));
        comboBox_NameOfMainEngineer->setMinimumSize(QSize(90, 20));
        comboBox_NameOfMainEngineer->setMaximumSize(QSize(180, 20));
        comboBox_NameOfMainEngineer->setFont(font1);
        comboBox_NameOfMainEngineer->setEditable(true);
        comboBox_NameOfMainEngineer->setMaxVisibleItems(40);
        comboBox_NameOfMainEngineer->setDuplicatesEnabled(false);
        comboBox_NameOfMainEngineer->setFrame(true);
        comboBox_NameOfMainEngineer->setModelColumn(0);

        gridLayout_4->addWidget(comboBox_NameOfMainEngineer, 1, 1, 1, 1);

        pushButton_2 = new QPushButton(groupBox_8);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));
        pushButton_2->setMinimumSize(QSize(20, 20));
        pushButton_2->setMaximumSize(QSize(20, 20));

        gridLayout_4->addWidget(pushButton_2, 1, 2, 1, 1);

        label_26 = new QLabel(groupBox_8);
        label_26->setObjectName(QStringLiteral("label_26"));
        label_26->setFont(font1);

        gridLayout_4->addWidget(label_26, 2, 0, 1, 1);

        comboBox_NameOfHeatProvider = new QComboBox(groupBox_8);
        comboBox_NameOfHeatProvider->setObjectName(QStringLiteral("comboBox_NameOfHeatProvider"));
        comboBox_NameOfHeatProvider->setMinimumSize(QSize(90, 20));
        comboBox_NameOfHeatProvider->setMaximumSize(QSize(180, 20));
        comboBox_NameOfHeatProvider->setFont(font1);
        comboBox_NameOfHeatProvider->setEditable(true);
        comboBox_NameOfHeatProvider->setMaxVisibleItems(40);
        comboBox_NameOfHeatProvider->setDuplicatesEnabled(false);
        comboBox_NameOfHeatProvider->setFrame(true);
        comboBox_NameOfHeatProvider->setModelColumn(0);

        gridLayout_4->addWidget(comboBox_NameOfHeatProvider, 2, 1, 1, 1);

        pushButton_3 = new QPushButton(groupBox_8);
        pushButton_3->setObjectName(QStringLiteral("pushButton_3"));
        pushButton_3->setMinimumSize(QSize(20, 20));
        pushButton_3->setMaximumSize(QSize(20, 20));

        gridLayout_4->addWidget(pushButton_3, 2, 2, 1, 1);


        gridLayout_6->addWidget(groupBox_8, 0, 0, 1, 3);

        groupBox_2 = new QGroupBox(scrollAreaWidgetContents);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        groupBox_2->setMinimumSize(QSize(450, 84));
        groupBox_2->setMaximumSize(QSize(450, 84));
        groupBox_2->setFont(font);
        gridLayout_7 = new QGridLayout(groupBox_2);
        gridLayout_7->setSpacing(6);
        gridLayout_7->setContentsMargins(11, 11, 11, 11);
        gridLayout_7->setObjectName(QStringLiteral("gridLayout_7"));
        label_28 = new QLabel(groupBox_2);
        label_28->setObjectName(QStringLiteral("label_28"));
        label_28->setMinimumSize(QSize(82, 16));
        label_28->setFont(font1);

        gridLayout_7->addWidget(label_28, 0, 0, 1, 1);

        dateEdit_Project_Date = new QDateEdit(groupBox_2);
        dateEdit_Project_Date->setObjectName(QStringLiteral("dateEdit_Project_Date"));

        gridLayout_7->addWidget(dateEdit_Project_Date, 0, 1, 1, 2);

        label_29 = new QLabel(groupBox_2);
        label_29->setObjectName(QStringLiteral("label_29"));
        label_29->setFont(font1);

        gridLayout_7->addWidget(label_29, 1, 0, 1, 1);

        lineEdit_Project_Order_Number = new QLineEdit(groupBox_2);
        lineEdit_Project_Order_Number->setObjectName(QStringLiteral("lineEdit_Project_Order_Number"));
        lineEdit_Project_Order_Number->setMaximumSize(QSize(90, 20));

        gridLayout_7->addWidget(lineEdit_Project_Order_Number, 1, 1, 1, 1);

        checkBox = new QCheckBox(groupBox_2);
        checkBox->setObjectName(QStringLiteral("checkBox"));
        checkBox->setEnabled(false);
        checkBox->setFont(font1);

        gridLayout_7->addWidget(checkBox, 1, 2, 1, 1);


        gridLayout_6->addWidget(groupBox_2, 1, 0, 1, 2);

        groupBox_10 = new QGroupBox(scrollAreaWidgetContents);
        groupBox_10->setObjectName(QStringLiteral("groupBox_10"));
        groupBox_10->setMinimumSize(QSize(450, 82));
        groupBox_10->setMaximumSize(QSize(450, 82));
        groupBox_10->setFont(font);
        groupBox_10->setFlat(false);
        groupBox_10->setCheckable(true);
        groupBox_10->setChecked(true);
        gridLayout = new QGridLayout(groupBox_10);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        label_15 = new QLabel(groupBox_10);
        label_15->setObjectName(QStringLiteral("label_15"));
        label_15->setFont(font1);

        gridLayout->addWidget(label_15, 0, 0, 1, 1);

        lineEdit_CommenHeating_Square_OfAllFlats = new QLineEdit(groupBox_10);
        lineEdit_CommenHeating_Square_OfAllFlats->setObjectName(QStringLiteral("lineEdit_CommenHeating_Square_OfAllFlats"));
        lineEdit_CommenHeating_Square_OfAllFlats->setMinimumSize(QSize(7, 20));
        lineEdit_CommenHeating_Square_OfAllFlats->setMaximumSize(QSize(100, 20));

        gridLayout->addWidget(lineEdit_CommenHeating_Square_OfAllFlats, 0, 1, 1, 1);

        label_30 = new QLabel(groupBox_10);
        label_30->setObjectName(QStringLiteral("label_30"));
        label_30->setFont(font1);

        gridLayout->addWidget(label_30, 0, 2, 1, 1);

        label_27 = new QLabel(groupBox_10);
        label_27->setObjectName(QStringLiteral("label_27"));
        label_27->setFont(font1);

        gridLayout->addWidget(label_27, 1, 0, 1, 1);

        lineEdit_Commen_Heating_Square_Of_MZK = new QLineEdit(groupBox_10);
        lineEdit_Commen_Heating_Square_Of_MZK->setObjectName(QStringLiteral("lineEdit_Commen_Heating_Square_Of_MZK"));
        lineEdit_Commen_Heating_Square_Of_MZK->setMinimumSize(QSize(0, 20));
        lineEdit_Commen_Heating_Square_Of_MZK->setMaximumSize(QSize(100, 20));

        gridLayout->addWidget(lineEdit_Commen_Heating_Square_Of_MZK, 1, 1, 1, 1);

        label_31 = new QLabel(groupBox_10);
        label_31->setObjectName(QStringLiteral("label_31"));
        label_31->setFont(font1);

        gridLayout->addWidget(label_31, 1, 2, 1, 1);


        gridLayout_6->addWidget(groupBox_10, 2, 0, 1, 2);

        groupBox_9 = new QGroupBox(scrollAreaWidgetContents);
        groupBox_9->setObjectName(QStringLiteral("groupBox_9"));
        groupBox_9->setMinimumSize(QSize(450, 114));
        groupBox_9->setMaximumSize(QSize(450, 114));
        groupBox_9->setFont(font);
        gridLayout_14 = new QGridLayout(groupBox_9);
        gridLayout_14->setSpacing(6);
        gridLayout_14->setContentsMargins(11, 11, 11, 11);
        gridLayout_14->setObjectName(QStringLiteral("gridLayout_14"));
        checkBox_5 = new QCheckBox(groupBox_9);
        checkBox_5->setObjectName(QStringLiteral("checkBox_5"));
        checkBox_5->setMinimumSize(QSize(275, 17));
        checkBox_5->setFont(font1);
        checkBox_5->setChecked(true);

        gridLayout_14->addWidget(checkBox_5, 0, 0, 2, 2);

        lineEdit_AparatementFloor = new QLineEdit(groupBox_9);
        lineEdit_AparatementFloor->setObjectName(QStringLiteral("lineEdit_AparatementFloor"));
        lineEdit_AparatementFloor->setMinimumSize(QSize(60, 20));
        lineEdit_AparatementFloor->setMaximumSize(QSize(16777215, 20));

        gridLayout_14->addWidget(lineEdit_AparatementFloor, 1, 1, 1, 1);

        checkBox_6 = new QCheckBox(groupBox_9);
        checkBox_6->setObjectName(QStringLiteral("checkBox_6"));
        checkBox_6->setMinimumSize(QSize(275, 17));
        checkBox_6->setFont(font1);
        checkBox_6->setChecked(true);

        gridLayout_14->addWidget(checkBox_6, 2, 0, 1, 1);

        lineEdit_FloorWhereMeterIsMounted = new QLineEdit(groupBox_9);
        lineEdit_FloorWhereMeterIsMounted->setObjectName(QStringLiteral("lineEdit_FloorWhereMeterIsMounted"));
        lineEdit_FloorWhereMeterIsMounted->setMinimumSize(QSize(60, 20));
        lineEdit_FloorWhereMeterIsMounted->setMaximumSize(QSize(16777215, 20));

        gridLayout_14->addWidget(lineEdit_FloorWhereMeterIsMounted, 2, 1, 1, 1);

        checkBox_7 = new QCheckBox(groupBox_9);
        checkBox_7->setObjectName(QStringLiteral("checkBox_7"));
        checkBox_7->setMinimumSize(QSize(275, 17));
        checkBox_7->setFont(font1);
        checkBox_7->setChecked(true);

        gridLayout_14->addWidget(checkBox_7, 3, 0, 1, 1);

        lineEdit_FloorCount = new QLineEdit(groupBox_9);
        lineEdit_FloorCount->setObjectName(QStringLiteral("lineEdit_FloorCount"));
        lineEdit_FloorCount->setMinimumSize(QSize(60, 20));
        lineEdit_FloorCount->setMaximumSize(QSize(16777215, 20));

        gridLayout_14->addWidget(lineEdit_FloorCount, 3, 1, 1, 1);


        gridLayout_6->addWidget(groupBox_9, 3, 0, 1, 2);

        groupBox_6 = new QGroupBox(scrollAreaWidgetContents);
        groupBox_6->setObjectName(QStringLiteral("groupBox_6"));
        groupBox_6->setMinimumSize(QSize(451, 81));
        groupBox_6->setMaximumSize(QSize(451, 81));
        groupBox_6->setFont(font);
        gridLayout_11 = new QGridLayout(groupBox_6);
        gridLayout_11->setSpacing(6);
        gridLayout_11->setContentsMargins(11, 11, 11, 11);
        gridLayout_11->setObjectName(QStringLiteral("gridLayout_11"));
        label_17 = new QLabel(groupBox_6);
        label_17->setObjectName(QStringLiteral("label_17"));
        label_17->setMinimumSize(QSize(210, 22));
        label_17->setFont(font1);

        gridLayout_11->addWidget(label_17, 0, 0, 1, 1);

        lineEdit_HeatingSquare = new QLineEdit(groupBox_6);
        lineEdit_HeatingSquare->setObjectName(QStringLiteral("lineEdit_HeatingSquare"));
        lineEdit_HeatingSquare->setMinimumSize(QSize(66, 20));
        lineEdit_HeatingSquare->setMaximumSize(QSize(16777215, 20));

        gridLayout_11->addWidget(lineEdit_HeatingSquare, 0, 1, 1, 1);

        label_18 = new QLabel(groupBox_6);
        label_18->setObjectName(QStringLiteral("label_18"));
        QFont font2;
        font2.setBold(false);
        font2.setWeight(50);
        label_18->setFont(font2);

        gridLayout_11->addWidget(label_18, 0, 2, 1, 1);

        label_19 = new QLabel(groupBox_6);
        label_19->setObjectName(QStringLiteral("label_19"));
        label_19->setFont(font1);

        gridLayout_11->addWidget(label_19, 1, 0, 1, 1);

        lineEdit_CommenSquare = new QLineEdit(groupBox_6);
        lineEdit_CommenSquare->setObjectName(QStringLiteral("lineEdit_CommenSquare"));
        lineEdit_CommenSquare->setMinimumSize(QSize(66, 20));
        lineEdit_CommenSquare->setMaximumSize(QSize(16777215, 20));

        gridLayout_11->addWidget(lineEdit_CommenSquare, 1, 1, 1, 1);

        label_20 = new QLabel(groupBox_6);
        label_20->setObjectName(QStringLiteral("label_20"));
        label_20->setFont(font2);

        gridLayout_11->addWidget(label_20, 1, 2, 1, 1);


        gridLayout_6->addWidget(groupBox_6, 4, 0, 1, 2);

        groupBox_7 = new QGroupBox(scrollAreaWidgetContents);
        groupBox_7->setObjectName(QStringLiteral("groupBox_7"));
        groupBox_7->setMinimumSize(QSize(220, 134));
        groupBox_7->setMaximumSize(QSize(220, 134));
        groupBox_7->setFont(font);
        gridLayout_3 = new QGridLayout(groupBox_7);
        gridLayout_3->setSpacing(6);
        gridLayout_3->setContentsMargins(11, 11, 11, 11);
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        label_21 = new QLabel(groupBox_7);
        label_21->setObjectName(QStringLiteral("label_21"));
        label_21->setMinimumSize(QSize(80, 16));
        label_21->setFont(font1);

        gridLayout_3->addWidget(label_21, 0, 0, 1, 1);

        comboBox_MeterFlowRate = new QComboBox(groupBox_7);
        comboBox_MeterFlowRate->setObjectName(QStringLiteral("comboBox_MeterFlowRate"));
        comboBox_MeterFlowRate->setMinimumSize(QSize(90, 20));
        comboBox_MeterFlowRate->setMaximumSize(QSize(90, 20));
        comboBox_MeterFlowRate->setFont(font1);
        comboBox_MeterFlowRate->setFrame(true);

        gridLayout_3->addWidget(comboBox_MeterFlowRate, 0, 1, 1, 1);

        label_22 = new QLabel(groupBox_7);
        label_22->setObjectName(QStringLiteral("label_22"));
        label_22->setMinimumSize(QSize(80, 16));
        label_22->setFont(font1);

        gridLayout_3->addWidget(label_22, 1, 0, 1, 1);

        comboBox_Dn = new QComboBox(groupBox_7);
        comboBox_Dn->setObjectName(QStringLiteral("comboBox_Dn"));
        comboBox_Dn->setMinimumSize(QSize(90, 20));
        comboBox_Dn->setMaximumSize(QSize(90, 20));
        comboBox_Dn->setFont(font1);
        comboBox_Dn->setFrame(true);

        gridLayout_3->addWidget(comboBox_Dn, 1, 1, 1, 1);

        label_23 = new QLabel(groupBox_7);
        label_23->setObjectName(QStringLiteral("label_23"));
        label_23->setMinimumSize(QSize(82, 16));
        label_23->setFont(font1);

        gridLayout_3->addWidget(label_23, 2, 0, 1, 1);

        comboBox_Pipe = new QComboBox(groupBox_7);
        comboBox_Pipe->setObjectName(QStringLiteral("comboBox_Pipe"));
        comboBox_Pipe->setMinimumSize(QSize(90, 20));
        comboBox_Pipe->setMaximumSize(QSize(16777215, 20));
        comboBox_Pipe->setFont(font1);
        comboBox_Pipe->setFrame(true);

        gridLayout_3->addWidget(comboBox_Pipe, 2, 1, 1, 1);

        label_32 = new QLabel(groupBox_7);
        label_32->setObjectName(QStringLiteral("label_32"));
        label_32->setEnabled(false);
        label_32->setMinimumSize(QSize(82, 16));
        label_32->setFont(font1);

        gridLayout_3->addWidget(label_32, 3, 0, 1, 1);

        lineEdit_HeatEnergyProvider_3 = new QLineEdit(groupBox_7);
        lineEdit_HeatEnergyProvider_3->setObjectName(QStringLiteral("lineEdit_HeatEnergyProvider_3"));
        lineEdit_HeatEnergyProvider_3->setEnabled(false);
        lineEdit_HeatEnergyProvider_3->setMaximumSize(QSize(90, 20));

        gridLayout_3->addWidget(lineEdit_HeatEnergyProvider_3, 3, 1, 1, 1);


        gridLayout_6->addWidget(groupBox_7, 5, 0, 1, 1);

        groupBox_4 = new QGroupBox(scrollAreaWidgetContents);
        groupBox_4->setObjectName(QStringLiteral("groupBox_4"));
        groupBox_4->setMinimumSize(QSize(220, 111));
        groupBox_4->setMaximumSize(QSize(11111, 111));
        groupBox_4->setFont(font);
        gridLayout_5 = new QGridLayout(groupBox_4);
        gridLayout_5->setSpacing(6);
        gridLayout_5->setContentsMargins(11, 11, 11, 11);
        gridLayout_5->setObjectName(QStringLiteral("gridLayout_5"));
        label_13 = new QLabel(groupBox_4);
        label_13->setObjectName(QStringLiteral("label_13"));
        label_13->setFont(font1);

        gridLayout_5->addWidget(label_13, 0, 0, 1, 1);

        lineEdit_City = new QLineEdit(groupBox_4);
        lineEdit_City->setObjectName(QStringLiteral("lineEdit_City"));
        lineEdit_City->setMaximumSize(QSize(16777215, 20));
        lineEdit_City->setFont(font2);

        gridLayout_5->addWidget(lineEdit_City, 0, 1, 1, 1);

        label_14 = new QLabel(groupBox_4);
        label_14->setObjectName(QStringLiteral("label_14"));
        label_14->setFont(font1);

        gridLayout_5->addWidget(label_14, 1, 0, 1, 1);

        lineEdit_Street = new QLineEdit(groupBox_4);
        lineEdit_Street->setObjectName(QStringLiteral("lineEdit_Street"));
        lineEdit_Street->setMaximumSize(QSize(16777215, 20));

        gridLayout_5->addWidget(lineEdit_Street, 1, 1, 1, 1);

        label_16 = new QLabel(groupBox_4);
        label_16->setObjectName(QStringLiteral("label_16"));
        label_16->setFont(font1);

        gridLayout_5->addWidget(label_16, 2, 0, 1, 1);

        lineEdit_Apartment = new QLineEdit(groupBox_4);
        lineEdit_Apartment->setObjectName(QStringLiteral("lineEdit_Apartment"));
        lineEdit_Apartment->setMaximumSize(QSize(16777215, 20));

        gridLayout_5->addWidget(lineEdit_Apartment, 2, 1, 1, 1);


        gridLayout_6->addWidget(groupBox_4, 5, 1, 1, 2);

        groupBox_5 = new QGroupBox(scrollAreaWidgetContents);
        groupBox_5->setObjectName(QStringLiteral("groupBox_5"));
        groupBox_5->setEnabled(false);
        groupBox_5->setMinimumSize(QSize(220, 56));
        groupBox_5->setMaximumSize(QSize(220, 56));
        groupBox_5->setFont(font);
        gridLayout_9 = new QGridLayout(groupBox_5);
        gridLayout_9->setSpacing(6);
        gridLayout_9->setContentsMargins(11, 11, 11, 11);
        gridLayout_9->setObjectName(QStringLiteral("gridLayout_9"));
        radioButton_3 = new QRadioButton(groupBox_5);
        buttonGroup = new QButtonGroup(mainwindowClass);
        buttonGroup->setObjectName(QStringLiteral("buttonGroup"));
        buttonGroup->addButton(radioButton_3);
        radioButton_3->setObjectName(QStringLiteral("radioButton_3"));
        radioButton_3->setFont(font1);
        radioButton_3->setChecked(true);

        gridLayout_9->addWidget(radioButton_3, 0, 0, 1, 1);

        radioButton_4 = new QRadioButton(groupBox_5);
        buttonGroup->addButton(radioButton_4);
        radioButton_4->setObjectName(QStringLiteral("radioButton_4"));
        radioButton_4->setFont(font1);

        gridLayout_9->addWidget(radioButton_4, 0, 1, 1, 1);


        gridLayout_6->addWidget(groupBox_5, 6, 0, 1, 1);

        pushButton_Save = new QPushButton(scrollAreaWidgetContents);
        pushButton_Save->setObjectName(QStringLiteral("pushButton_Save"));
        pushButton_Save->setMaximumSize(QSize(100, 16777215));

        gridLayout_6->addWidget(pushButton_Save, 6, 2, 1, 1);

        pushButton_Print = new QPushButton(scrollAreaWidgetContents);
        pushButton_Print->setObjectName(QStringLiteral("pushButton_Print"));
        pushButton_Print->setEnabled(true);
        pushButton_Print->setMaximumSize(QSize(100, 16777215));

        gridLayout_6->addWidget(pushButton_Print, 6, 3, 1, 1);

        scrollArea->setWidget(scrollAreaWidgetContents);

        gridLayout_2->addWidget(scrollArea, 0, 0, 1, 1);

        mainwindowClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(mainwindowClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 740, 21));
        menu = new QMenu(menuBar);
        menu->setObjectName(QStringLiteral("menu"));
        menu_settings = new QMenu(menuBar);
        menu_settings->setObjectName(QStringLiteral("menu_settings"));
        menu_3 = new QMenu(menuBar);
        menu_3->setObjectName(QStringLiteral("menu_3"));
        mainwindowClass->setMenuBar(menuBar);
        mainToolBar = new QToolBar(mainwindowClass);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        mainwindowClass->addToolBar(Qt::TopToolBarArea, mainToolBar);

        menuBar->addAction(menu->menuAction());
        menuBar->addAction(menu_settings->menuAction());
        menuBar->addAction(menu_3->menuAction());
        menu_3->addAction(actionDev);

        retranslateUi(mainwindowClass);

        comboBox_NameOfOrganisationWhatSignProject->setCurrentIndex(-1);


        QMetaObject::connectSlotsByName(mainwindowClass);
    } // setupUi

    void retranslateUi(QMainWindow *mainwindowClass)
    {
        mainwindowClass->setWindowTitle(QApplication::translate("mainwindowClass", "HeatProjectMaker 0.1_DEBUG", 0));
        actionDev->setText(QApplication::translate("mainwindowClass", "Dev", 0));
        groupBox_8->setTitle(QApplication::translate("mainwindowClass", "\320\224\320\260\320\275\320\275\321\213\320\271 \320\276 \320\277\320\276\321\201\321\202\320\260\320\262\321\211\320\270\320\272\320\265 \321\202\320\265\320\277\320\273\320\260:", 0));
        label_24->setText(QApplication::translate("mainwindowClass", "\320\235\320\260\320\267\320\262\320\260\320\275\320\270\320\265 \320\276\321\200\320\263\320\260\320\275\320\270\320\267\320\260\321\206\320\270\320\270 \321\201\320\276\320\263\320\273\320\260\321\201\320\276\320\262\321\203\321\216\321\211\320\265\320\271 \320\277\321\200\320\276\320\265\320\272\321\202:", 0));
        pushButton->setText(QApplication::translate("mainwindowClass", "+", 0));
        label_25->setText(QApplication::translate("mainwindowClass", "\320\244.\320\230.\320\236. \320\223\320\273\320\260\320\262\320\275\320\276\320\263\320\276 \320\270\320\275\320\266\320\265\320\275\320\265\321\200\320\260:", 0));
        pushButton_2->setText(QApplication::translate("mainwindowClass", "+", 0));
        label_26->setText(QApplication::translate("mainwindowClass", "\320\235\320\260\320\267\320\262\320\260\320\275\320\270\320\265 \320\276\321\200\320\263\320\260\320\275\320\270\320\267\320\260\321\206\320\270\320\270 \320\277\320\276\321\201\321\202\320\260\320\262\320\273\321\217\321\216\321\211\320\265\320\271  \321\202\320\265\320\277\320\273\320\276:", 0));
        comboBox_NameOfHeatProvider->clear();
        comboBox_NameOfHeatProvider->insertItems(0, QStringList()
         << QApplication::translate("mainwindowClass", "\320\237\320\220\320\242 \320\232\320\270\321\227\320\262\320\265\320\275\320\265\321\200\320\263\320\276", 0)
        );
        pushButton_3->setText(QApplication::translate("mainwindowClass", "+", 0));
        groupBox_2->setTitle(QApplication::translate("mainwindowClass", "\320\224\320\260\321\202\320\260 \320\270 \320\275\320\276\320\274\320\265\321\200 \320\277\321\200\320\276\320\265\320\272\321\202\320\260:", 0));
        label_28->setText(QApplication::translate("mainwindowClass", "\320\224\320\260\321\202\320\260 \321\200\320\260\320\267\321\200\320\260\320\261\320\276\321\202\320\272\320\270 \320\277\321\200\320\276\320\265\320\272\321\202\320\260:", 0));
        label_29->setText(QApplication::translate("mainwindowClass", "\320\237\320\276\321\200\321\217\320\264\320\272\320\276\320\262\321\213\320\271 \342\204\226 \320\277\321\200\320\276\320\265\320\272\321\202\320\260:", 0));
        checkBox->setText(QApplication::translate("mainwindowClass", "\320\235\320\260\320\267\320\275\320\260\321\207\320\270\321\202\321\214 \320\260\320\262\321\202\320\276\320\274\320\260\321\202\320\270\321\207\320\265\321\201\320\272\320\270", 0));
        groupBox_10->setTitle(QApplication::translate("mainwindowClass", "\320\224\320\260\320\275\320\275\321\213\320\265 \320\264\320\273\321\217 \321\200\320\260\321\201\321\207\320\265\321\202\320\260 \320\234\320\227\320\232:", 0));
        label_15->setText(QApplication::translate("mainwindowClass", "\320\241\321\203\320\274\320\274\320\260 \320\276\321\202\320\260\320\277\320\273\320\270\320\262\320\260\320\265\320\274\321\213\321\205 \320\277\320\273\320\276\321\211\320\260\320\264\320\265\320\271 \320\272\320\262\320\260\321\200\321\202\320\270\321\200 \320\262 \320\264\320\276\320\274\320\265:", 0));
        lineEdit_CommenHeating_Square_OfAllFlats->setText(QString());
        label_30->setText(QApplication::translate("mainwindowClass", "\320\2742", 0));
        label_27->setText(QApplication::translate("mainwindowClass", "\320\241\321\203\320\274\320\274\320\260 \320\276\321\202\320\260\320\277\320\273\320\270\320\262\320\260\320\265\320\274\321\213\321\205 \320\277\320\273\320\276\321\211\320\260\320\264\320\265\320\271 \320\234\320\227\320\232 \320\262 \320\264\320\276\320\274\320\265:", 0));
        lineEdit_Commen_Heating_Square_Of_MZK->setText(QString());
        label_31->setText(QApplication::translate("mainwindowClass", "\320\2742", 0));
        groupBox_9->setTitle(QApplication::translate("mainwindowClass", "\320\237\320\260\321\200\320\260\320\274\320\265\321\202\321\200\321\213 \320\274\320\265\321\201\321\202\320\260 \321\203\321\201\321\202\320\260\320\275\320\276\320\262\320\272\320\270:", 0));
        checkBox_5->setText(QApplication::translate("mainwindowClass", "\320\255\321\202\320\260\320\266 \320\275\320\260 \320\272\320\276\321\202\320\276\321\200\320\276\320\274 \321\200\320\260\321\201\320\277\320\276\320\273\320\276\320\266\320\265\320\275\320\260 \320\272\320\262\320\260\321\200\321\202\320\270\321\200\320\260:", 0));
        checkBox_6->setText(QApplication::translate("mainwindowClass", "\320\255\321\202\320\260\320\266 \320\275\320\260 \320\272\320\276\321\202\320\276\321\200\320\276\320\274 \321\203\321\201\321\202\320\260\320\275\320\276\320\273\320\265\320\275 \321\201\321\207\320\265\321\202\321\207\320\270\320\272:", 0));
        checkBox_7->setText(QApplication::translate("mainwindowClass", "\320\232\320\276\320\273\320\270\321\207\320\265\321\201\321\202\320\262\320\276 \321\215\321\202\320\260\320\266\320\265\320\271 \320\262 \320\264\320\276\320\274\320\265:", 0));
        groupBox_6->setTitle(QApplication::translate("mainwindowClass", "\320\237\320\273\320\276\321\211\320\260\320\264\320\270 \320\276\320\261\321\214\320\265\320\272\321\202\320\260:", 0));
        label_17->setText(QApplication::translate("mainwindowClass", "\320\236\321\202\320\260\320\277\320\273\320\270\320\262\320\260\320\265\320\274\320\260\321\217 \320\277\320\273\320\276\321\211\320\260\320\264\321\214 \320\272\320\262\320\260\321\200\321\202\320\270\321\200\321\213:", 0));
        label_18->setText(QApplication::translate("mainwindowClass", "\320\2742", 0));
        label_19->setText(QApplication::translate("mainwindowClass", "\320\236\320\261\321\211\320\260\321\217 \320\276\320\261\321\211\320\260\321\217 \320\272\320\262\320\260\321\200\321\202\320\270\321\200\321\213:", 0));
        label_20->setText(QApplication::translate("mainwindowClass", "\320\2742", 0));
        groupBox_7->setTitle(QApplication::translate("mainwindowClass", "\320\237\320\260\321\200\320\260\320\274\320\265\321\202\321\200\321\213 \321\201\321\207\320\265\321\202\321\207\320\270\320\272\320\260 \321\202\320\265\320\277\320\273\320\260:", 0));
        label_21->setText(QApplication::translate("mainwindowClass", "\320\240\320\260\321\201\321\205\320\276\320\264:", 0));
        comboBox_MeterFlowRate->clear();
        comboBox_MeterFlowRate->insertItems(0, QStringList()
         << QApplication::translate("mainwindowClass", "0.6", 0)
         << QApplication::translate("mainwindowClass", "1.5", 0)
         << QApplication::translate("mainwindowClass", "2.5", 0)
        );
        label_22->setText(QApplication::translate("mainwindowClass", "\320\224\320\270\320\260\320\274\320\265\321\202\321\200:", 0));
        comboBox_Dn->clear();
        comboBox_Dn->insertItems(0, QStringList()
         << QApplication::translate("mainwindowClass", "15", 0)
         << QApplication::translate("mainwindowClass", "20", 0)
        );
        label_23->setText(QApplication::translate("mainwindowClass", "\320\242\321\200\321\203\320\261\320\276\320\277\321\200\320\276\320\262\320\276\320\264:", 0));
        comboBox_Pipe->clear();
        comboBox_Pipe->insertItems(0, QStringList()
         << QApplication::translate("mainwindowClass", "\320\237\320\276\320\264\320\260\321\216\321\211\320\270\320\271", 0)
         << QApplication::translate("mainwindowClass", "\320\236\320\261\321\200\320\260\321\202\320\275\321\213\320\271", 0)
        );
        label_32->setText(QApplication::translate("mainwindowClass", "\320\227\320\260\320\262\320\276\320\264\321\201\320\272\320\276\320\271 \342\204\226:", 0));
        lineEdit_HeatEnergyProvider_3->setText(QApplication::translate("mainwindowClass", "65833342", 0));
        groupBox_4->setTitle(QApplication::translate("mainwindowClass", "\320\220\320\264\321\200\320\265\321\201\321\201 \320\276\320\261\321\214\320\265\320\272\321\202\320\260:", 0));
        label_13->setText(QApplication::translate("mainwindowClass", "\320\223\320\276\321\200\320\276\320\264:", 0));
        lineEdit_City->setText(QApplication::translate("mainwindowClass", "\320\232\320\270\321\227\320\262", 0));
        label_14->setText(QApplication::translate("mainwindowClass", "\320\243\320\273\320\270\321\206\320\260:", 0));
        label_16->setText(QApplication::translate("mainwindowClass", "\320\232\320\262\320\260\321\200\321\202\320\270\321\200\320\260:", 0));
        groupBox_5->setTitle(QApplication::translate("mainwindowClass", "\320\234\320\265\321\201\321\202\320\276 \321\203\321\201\321\202\320\260\320\275\320\276\320\262\320\272\320\270:", 0));
        radioButton_3->setText(QApplication::translate("mainwindowClass", "\320\222 \320\272\320\262\320\260\321\200\321\202\320\270\321\200\320\265", 0));
        radioButton_4->setText(QApplication::translate("mainwindowClass", "\320\235\320\260 \321\215\321\202\320\260\320\266\320\265", 0));
        pushButton_Save->setText(QApplication::translate("mainwindowClass", "\320\241\320\276\321\205\321\200\320\260\320\275\320\270\321\202\321\214", 0));
        pushButton_Print->setText(QApplication::translate("mainwindowClass", "\320\240\320\260\321\201\320\277\320\265\321\207\320\260\321\202\320\260\321\202\321\214", 0));
        menu->setTitle(QApplication::translate("mainwindowClass", "\320\244\320\260\320\271\320\273", 0));
        menu_settings->setTitle(QApplication::translate("mainwindowClass", "\320\235\320\260\321\201\321\202\321\200\320\276\320\271\320\272\320\270", 0));
        menu_3->setTitle(QApplication::translate("mainwindowClass", "\320\236 \320\237\321\200\320\276\320\263\321\200\320\260\320\274\320\274\320\265", 0));
    } // retranslateUi

};

namespace Ui {
    class mainwindowClass: public Ui_mainwindowClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
