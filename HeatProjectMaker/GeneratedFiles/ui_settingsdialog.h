/********************************************************************************
** Form generated from reading UI file 'settingsdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.6.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SETTINGSDIALOG_H
#define UI_SETTINGSDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>

QT_BEGIN_NAMESPACE

class Ui_SettingsDialog
{
public:
    QGridLayout *gridLayout_14;
    QGroupBox *groupBox;
    QGridLayout *gridLayout_13;
    QGridLayout *gridLayout_12;
    QGridLayout *gridLayout;
    QLabel *label_3;
    QLineEdit *lineEdit_Calc_Temp_Of_Media_In_Heating_Period_Flow_Pipe;
    QGridLayout *gridLayout_3;
    QLabel *label_4;
    QLineEdit *lineEdit_Calc_Temp_Of_Media_In_Heating_Period_Return_Pipe;
    QGridLayout *gridLayout_4;
    QLabel *label_2;
    QLineEdit *lineEdit_Calculated_Temp_Out_Air_Heating_Sys;
    QGridLayout *gridLayout_5;
    QLabel *label;
    QLineEdit *lineEdit_Middle_Temp_Inner_Air_Heating_Flat;
    QGridLayout *gridLayout_6;
    QLabel *label_Cal;
    QLineEdit *lineEdit_Calculated_Temp_Out_Air_In_Transition_Period;
    QGridLayout *gridLayout_7;
    QLabel *label_Temp_Of_HeatingMedia_In_Flow_Pipe_In_Transition_Period_HeatinSys;
    QLineEdit *lineEdit_lineEdit_Temp_HeatMedia_In_Flow_Pipe;
    QGridLayout *gridLayout_8;
    QLabel *label_6;
    QLineEdit *lineEdit_Temp_HeatMedia_In_Return_Pipe;
    QGridLayout *gridLayout_9;
    QLabel *label_15;
    QLineEdit *lineEdit_Temp_HeatingMedia_In_Return_Pipe_For_Calculated_Temp_8;
    QGridLayout *gridLayout_11;
    QLabel *label_17;
    QLineEdit *lineEdit_Water_Heat_Capacity;
    QFrame *line;
    QGridLayout *gridLayout_2;
    QLineEdit *lineEdit_Temp4;
    QLabel *label_8;
    QLabel *label_13;
    QLineEdit *lineEdit_DensityOfHeatMediaPerTemp3;
    QLabel *label_20;
    QLineEdit *lineEdit_Temp3;
    QLabel *label_7;
    QLineEdit *lineEdit_DensityOfHeatMediaPerTemp2;
    QLabel *label_18;
    QLineEdit *lineEdit_Temp1;
    QLabel *label_21;
    QLineEdit *lineEdit_Temp2;
    QLineEdit *lineEdit_DensityOfHeatMediaPerTemp4;
    QLabel *label_10;
    QLabel *label_12;
    QLabel *label_19;
    QLineEdit *lineEdit_DensityOfHeatMediaPerTemp1;
    QSpacerItem *verticalSpacer;
    QLabel *label_9;
    QLabel *label_14;
    QLabel *label_11;
    QSpacerItem *horizontalSpacer;
    QGridLayout *gridLayout_10;
    QPushButton *pushButton_Save;
    QPushButton *pushButton_Cancel;
    QSpacerItem *horizontalSpacer_2;

    void setupUi(QDialog *SettingsDialog)
    {
        if (SettingsDialog->objectName().isEmpty())
            SettingsDialog->setObjectName(QStringLiteral("SettingsDialog"));
        SettingsDialog->setWindowModality(Qt::ApplicationModal);
        SettingsDialog->resize(641, 475);
        SettingsDialog->setModal(true);
        gridLayout_14 = new QGridLayout(SettingsDialog);
        gridLayout_14->setSpacing(6);
        gridLayout_14->setContentsMargins(11, 11, 11, 11);
        gridLayout_14->setObjectName(QStringLiteral("gridLayout_14"));
        groupBox = new QGroupBox(SettingsDialog);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        groupBox->setMinimumSize(QSize(623, 457));
        QFont font;
        font.setPointSize(10);
        groupBox->setFont(font);
        groupBox->setFlat(false);
        groupBox->setCheckable(false);
        groupBox->setChecked(false);
        gridLayout_13 = new QGridLayout(groupBox);
        gridLayout_13->setSpacing(6);
        gridLayout_13->setContentsMargins(11, 11, 11, 11);
        gridLayout_13->setObjectName(QStringLiteral("gridLayout_13"));
        gridLayout_12 = new QGridLayout();
        gridLayout_12->setSpacing(6);
        gridLayout_12->setObjectName(QStringLiteral("gridLayout_12"));
        gridLayout = new QGridLayout();
        gridLayout->setSpacing(6);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        label_3 = new QLabel(groupBox);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setMinimumSize(QSize(0, 0));

        gridLayout->addWidget(label_3, 0, 0, 1, 1);

        lineEdit_Calc_Temp_Of_Media_In_Heating_Period_Flow_Pipe = new QLineEdit(groupBox);
        lineEdit_Calc_Temp_Of_Media_In_Heating_Period_Flow_Pipe->setObjectName(QStringLiteral("lineEdit_Calc_Temp_Of_Media_In_Heating_Period_Flow_Pipe"));
        lineEdit_Calc_Temp_Of_Media_In_Heating_Period_Flow_Pipe->setMaximumSize(QSize(60, 16777215));

        gridLayout->addWidget(lineEdit_Calc_Temp_Of_Media_In_Heating_Period_Flow_Pipe, 0, 1, 1, 1);


        gridLayout_12->addLayout(gridLayout, 0, 0, 1, 1);

        gridLayout_3 = new QGridLayout();
        gridLayout_3->setSpacing(6);
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        label_4 = new QLabel(groupBox);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setMinimumSize(QSize(0, 0));

        gridLayout_3->addWidget(label_4, 0, 0, 1, 1);

        lineEdit_Calc_Temp_Of_Media_In_Heating_Period_Return_Pipe = new QLineEdit(groupBox);
        lineEdit_Calc_Temp_Of_Media_In_Heating_Period_Return_Pipe->setObjectName(QStringLiteral("lineEdit_Calc_Temp_Of_Media_In_Heating_Period_Return_Pipe"));
        lineEdit_Calc_Temp_Of_Media_In_Heating_Period_Return_Pipe->setMaximumSize(QSize(60, 16777215));

        gridLayout_3->addWidget(lineEdit_Calc_Temp_Of_Media_In_Heating_Period_Return_Pipe, 0, 1, 1, 1);


        gridLayout_12->addLayout(gridLayout_3, 1, 0, 1, 1);

        gridLayout_4 = new QGridLayout();
        gridLayout_4->setSpacing(6);
        gridLayout_4->setObjectName(QStringLiteral("gridLayout_4"));
        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setMinimumSize(QSize(100, 0));
        label_2->setFont(font);

        gridLayout_4->addWidget(label_2, 0, 0, 1, 1);

        lineEdit_Calculated_Temp_Out_Air_Heating_Sys = new QLineEdit(groupBox);
        lineEdit_Calculated_Temp_Out_Air_Heating_Sys->setObjectName(QStringLiteral("lineEdit_Calculated_Temp_Out_Air_Heating_Sys"));
        lineEdit_Calculated_Temp_Out_Air_Heating_Sys->setMinimumSize(QSize(60, 20));
        lineEdit_Calculated_Temp_Out_Air_Heating_Sys->setMaximumSize(QSize(60, 20));

        gridLayout_4->addWidget(lineEdit_Calculated_Temp_Out_Air_Heating_Sys, 0, 1, 1, 1);


        gridLayout_12->addLayout(gridLayout_4, 2, 0, 1, 1);

        gridLayout_5 = new QGridLayout();
        gridLayout_5->setSpacing(6);
        gridLayout_5->setObjectName(QStringLiteral("gridLayout_5"));
        label = new QLabel(groupBox);
        label->setObjectName(QStringLiteral("label"));
        label->setMinimumSize(QSize(100, 0));
        label->setFont(font);

        gridLayout_5->addWidget(label, 0, 0, 1, 1);

        lineEdit_Middle_Temp_Inner_Air_Heating_Flat = new QLineEdit(groupBox);
        lineEdit_Middle_Temp_Inner_Air_Heating_Flat->setObjectName(QStringLiteral("lineEdit_Middle_Temp_Inner_Air_Heating_Flat"));
        lineEdit_Middle_Temp_Inner_Air_Heating_Flat->setMinimumSize(QSize(60, 20));
        lineEdit_Middle_Temp_Inner_Air_Heating_Flat->setMaximumSize(QSize(60, 20));

        gridLayout_5->addWidget(lineEdit_Middle_Temp_Inner_Air_Heating_Flat, 0, 1, 1, 1);


        gridLayout_12->addLayout(gridLayout_5, 3, 0, 1, 1);

        gridLayout_6 = new QGridLayout();
        gridLayout_6->setSpacing(6);
        gridLayout_6->setObjectName(QStringLiteral("gridLayout_6"));
        label_Cal = new QLabel(groupBox);
        label_Cal->setObjectName(QStringLiteral("label_Cal"));
        label_Cal->setMinimumSize(QSize(100, 0));
        label_Cal->setFont(font);

        gridLayout_6->addWidget(label_Cal, 0, 0, 1, 1);

        lineEdit_Calculated_Temp_Out_Air_In_Transition_Period = new QLineEdit(groupBox);
        lineEdit_Calculated_Temp_Out_Air_In_Transition_Period->setObjectName(QStringLiteral("lineEdit_Calculated_Temp_Out_Air_In_Transition_Period"));
        lineEdit_Calculated_Temp_Out_Air_In_Transition_Period->setMinimumSize(QSize(60, 20));
        lineEdit_Calculated_Temp_Out_Air_In_Transition_Period->setMaximumSize(QSize(60, 20));

        gridLayout_6->addWidget(lineEdit_Calculated_Temp_Out_Air_In_Transition_Period, 0, 1, 1, 1);


        gridLayout_12->addLayout(gridLayout_6, 4, 0, 1, 1);

        gridLayout_7 = new QGridLayout();
        gridLayout_7->setSpacing(6);
        gridLayout_7->setObjectName(QStringLiteral("gridLayout_7"));
        label_Temp_Of_HeatingMedia_In_Flow_Pipe_In_Transition_Period_HeatinSys = new QLabel(groupBox);
        label_Temp_Of_HeatingMedia_In_Flow_Pipe_In_Transition_Period_HeatinSys->setObjectName(QStringLiteral("label_Temp_Of_HeatingMedia_In_Flow_Pipe_In_Transition_Period_HeatinSys"));
        label_Temp_Of_HeatingMedia_In_Flow_Pipe_In_Transition_Period_HeatinSys->setMinimumSize(QSize(100, 0));
        label_Temp_Of_HeatingMedia_In_Flow_Pipe_In_Transition_Period_HeatinSys->setFont(font);

        gridLayout_7->addWidget(label_Temp_Of_HeatingMedia_In_Flow_Pipe_In_Transition_Period_HeatinSys, 0, 0, 1, 1);

        lineEdit_lineEdit_Temp_HeatMedia_In_Flow_Pipe = new QLineEdit(groupBox);
        lineEdit_lineEdit_Temp_HeatMedia_In_Flow_Pipe->setObjectName(QStringLiteral("lineEdit_lineEdit_Temp_HeatMedia_In_Flow_Pipe"));
        lineEdit_lineEdit_Temp_HeatMedia_In_Flow_Pipe->setMinimumSize(QSize(60, 20));
        lineEdit_lineEdit_Temp_HeatMedia_In_Flow_Pipe->setMaximumSize(QSize(60, 20));

        gridLayout_7->addWidget(lineEdit_lineEdit_Temp_HeatMedia_In_Flow_Pipe, 0, 1, 1, 1);


        gridLayout_12->addLayout(gridLayout_7, 5, 0, 1, 1);

        gridLayout_8 = new QGridLayout();
        gridLayout_8->setSpacing(6);
        gridLayout_8->setObjectName(QStringLiteral("gridLayout_8"));
        label_6 = new QLabel(groupBox);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setMinimumSize(QSize(100, 0));
        label_6->setFont(font);

        gridLayout_8->addWidget(label_6, 0, 0, 1, 1);

        lineEdit_Temp_HeatMedia_In_Return_Pipe = new QLineEdit(groupBox);
        lineEdit_Temp_HeatMedia_In_Return_Pipe->setObjectName(QStringLiteral("lineEdit_Temp_HeatMedia_In_Return_Pipe"));
        lineEdit_Temp_HeatMedia_In_Return_Pipe->setMinimumSize(QSize(60, 20));
        lineEdit_Temp_HeatMedia_In_Return_Pipe->setMaximumSize(QSize(60, 20));

        gridLayout_8->addWidget(lineEdit_Temp_HeatMedia_In_Return_Pipe, 0, 1, 1, 1);


        gridLayout_12->addLayout(gridLayout_8, 6, 0, 1, 1);

        gridLayout_9 = new QGridLayout();
        gridLayout_9->setSpacing(6);
        gridLayout_9->setObjectName(QStringLiteral("gridLayout_9"));
        label_15 = new QLabel(groupBox);
        label_15->setObjectName(QStringLiteral("label_15"));
        label_15->setMinimumSize(QSize(100, 0));
        label_15->setFont(font);

        gridLayout_9->addWidget(label_15, 0, 0, 1, 1);

        lineEdit_Temp_HeatingMedia_In_Return_Pipe_For_Calculated_Temp_8 = new QLineEdit(groupBox);
        lineEdit_Temp_HeatingMedia_In_Return_Pipe_For_Calculated_Temp_8->setObjectName(QStringLiteral("lineEdit_Temp_HeatingMedia_In_Return_Pipe_For_Calculated_Temp_8"));
        lineEdit_Temp_HeatingMedia_In_Return_Pipe_For_Calculated_Temp_8->setMinimumSize(QSize(60, 20));
        lineEdit_Temp_HeatingMedia_In_Return_Pipe_For_Calculated_Temp_8->setMaximumSize(QSize(60, 20));

        gridLayout_9->addWidget(lineEdit_Temp_HeatingMedia_In_Return_Pipe_For_Calculated_Temp_8, 0, 1, 1, 1);


        gridLayout_12->addLayout(gridLayout_9, 7, 0, 1, 1);

        gridLayout_11 = new QGridLayout();
        gridLayout_11->setSpacing(6);
        gridLayout_11->setObjectName(QStringLiteral("gridLayout_11"));
        label_17 = new QLabel(groupBox);
        label_17->setObjectName(QStringLiteral("label_17"));
        label_17->setMinimumSize(QSize(100, 0));
        label_17->setFont(font);

        gridLayout_11->addWidget(label_17, 0, 0, 1, 1);

        lineEdit_Water_Heat_Capacity = new QLineEdit(groupBox);
        lineEdit_Water_Heat_Capacity->setObjectName(QStringLiteral("lineEdit_Water_Heat_Capacity"));
        lineEdit_Water_Heat_Capacity->setMinimumSize(QSize(60, 20));
        lineEdit_Water_Heat_Capacity->setMaximumSize(QSize(60, 20));

        gridLayout_11->addWidget(lineEdit_Water_Heat_Capacity, 0, 1, 1, 1);


        gridLayout_12->addLayout(gridLayout_11, 8, 0, 1, 1);


        gridLayout_13->addLayout(gridLayout_12, 0, 0, 1, 1);

        line = new QFrame(groupBox);
        line->setObjectName(QStringLiteral("line"));
        line->setMinimumSize(QSize(0, 20));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        gridLayout_13->addWidget(line, 1, 0, 1, 1);

        gridLayout_2 = new QGridLayout();
        gridLayout_2->setSpacing(6);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        lineEdit_Temp4 = new QLineEdit(groupBox);
        lineEdit_Temp4->setObjectName(QStringLiteral("lineEdit_Temp4"));
        lineEdit_Temp4->setMinimumSize(QSize(51, 20));
        lineEdit_Temp4->setMaximumSize(QSize(60, 20));

        gridLayout_2->addWidget(lineEdit_Temp4, 4, 1, 1, 1);

        label_8 = new QLabel(groupBox);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setMinimumSize(QSize(40, 16));
        label_8->setMaximumSize(QSize(40, 16777215));

        gridLayout_2->addWidget(label_8, 1, 4, 1, 1);

        label_13 = new QLabel(groupBox);
        label_13->setObjectName(QStringLiteral("label_13"));
        label_13->setMinimumSize(QSize(20, 16));
        label_13->setMaximumSize(QSize(20, 16777215));

        gridLayout_2->addWidget(label_13, 4, 2, 1, 1);

        lineEdit_DensityOfHeatMediaPerTemp3 = new QLineEdit(groupBox);
        lineEdit_DensityOfHeatMediaPerTemp3->setObjectName(QStringLiteral("lineEdit_DensityOfHeatMediaPerTemp3"));
        lineEdit_DensityOfHeatMediaPerTemp3->setMinimumSize(QSize(61, 20));
        lineEdit_DensityOfHeatMediaPerTemp3->setMaximumSize(QSize(60, 20));

        gridLayout_2->addWidget(lineEdit_DensityOfHeatMediaPerTemp3, 3, 3, 1, 1);

        label_20 = new QLabel(groupBox);
        label_20->setObjectName(QStringLiteral("label_20"));
        label_20->setMinimumSize(QSize(235, 0));
        label_20->setMaximumSize(QSize(230, 16777215));
        label_20->setFont(font);

        gridLayout_2->addWidget(label_20, 3, 0, 1, 1);

        lineEdit_Temp3 = new QLineEdit(groupBox);
        lineEdit_Temp3->setObjectName(QStringLiteral("lineEdit_Temp3"));
        lineEdit_Temp3->setMinimumSize(QSize(51, 20));
        lineEdit_Temp3->setMaximumSize(QSize(60, 20));

        gridLayout_2->addWidget(lineEdit_Temp3, 3, 1, 1, 1);

        label_7 = new QLabel(groupBox);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setMinimumSize(QSize(20, 16));
        label_7->setMaximumSize(QSize(20, 16777215));

        gridLayout_2->addWidget(label_7, 1, 2, 1, 1);

        lineEdit_DensityOfHeatMediaPerTemp2 = new QLineEdit(groupBox);
        lineEdit_DensityOfHeatMediaPerTemp2->setObjectName(QStringLiteral("lineEdit_DensityOfHeatMediaPerTemp2"));
        lineEdit_DensityOfHeatMediaPerTemp2->setMinimumSize(QSize(61, 20));
        lineEdit_DensityOfHeatMediaPerTemp2->setMaximumSize(QSize(60, 20));

        gridLayout_2->addWidget(lineEdit_DensityOfHeatMediaPerTemp2, 2, 3, 1, 1);

        label_18 = new QLabel(groupBox);
        label_18->setObjectName(QStringLiteral("label_18"));
        label_18->setMinimumSize(QSize(235, 0));
        label_18->setMaximumSize(QSize(230, 16777215));
        label_18->setFont(font);

        gridLayout_2->addWidget(label_18, 1, 0, 1, 1);

        lineEdit_Temp1 = new QLineEdit(groupBox);
        lineEdit_Temp1->setObjectName(QStringLiteral("lineEdit_Temp1"));
        lineEdit_Temp1->setMinimumSize(QSize(51, 20));
        lineEdit_Temp1->setMaximumSize(QSize(60, 20));

        gridLayout_2->addWidget(lineEdit_Temp1, 1, 1, 1, 1);

        label_21 = new QLabel(groupBox);
        label_21->setObjectName(QStringLiteral("label_21"));
        label_21->setMinimumSize(QSize(235, 0));
        label_21->setMaximumSize(QSize(230, 16777215));
        label_21->setFont(font);

        gridLayout_2->addWidget(label_21, 4, 0, 1, 1);

        lineEdit_Temp2 = new QLineEdit(groupBox);
        lineEdit_Temp2->setObjectName(QStringLiteral("lineEdit_Temp2"));
        lineEdit_Temp2->setMinimumSize(QSize(51, 20));
        lineEdit_Temp2->setMaximumSize(QSize(60, 20));

        gridLayout_2->addWidget(lineEdit_Temp2, 2, 1, 1, 1);

        lineEdit_DensityOfHeatMediaPerTemp4 = new QLineEdit(groupBox);
        lineEdit_DensityOfHeatMediaPerTemp4->setObjectName(QStringLiteral("lineEdit_DensityOfHeatMediaPerTemp4"));
        lineEdit_DensityOfHeatMediaPerTemp4->setMinimumSize(QSize(61, 20));
        lineEdit_DensityOfHeatMediaPerTemp4->setMaximumSize(QSize(60, 20));

        gridLayout_2->addWidget(lineEdit_DensityOfHeatMediaPerTemp4, 4, 3, 1, 1);

        label_10 = new QLabel(groupBox);
        label_10->setObjectName(QStringLiteral("label_10"));
        label_10->setMinimumSize(QSize(40, 16));
        label_10->setMaximumSize(QSize(40, 16777215));

        gridLayout_2->addWidget(label_10, 2, 4, 1, 1);

        label_12 = new QLabel(groupBox);
        label_12->setObjectName(QStringLiteral("label_12"));
        label_12->setMinimumSize(QSize(40, 16));
        label_12->setMaximumSize(QSize(40, 16777215));

        gridLayout_2->addWidget(label_12, 3, 4, 1, 1);

        label_19 = new QLabel(groupBox);
        label_19->setObjectName(QStringLiteral("label_19"));
        label_19->setMinimumSize(QSize(235, 0));
        label_19->setMaximumSize(QSize(230, 16777215));
        label_19->setFont(font);

        gridLayout_2->addWidget(label_19, 2, 0, 1, 1);

        lineEdit_DensityOfHeatMediaPerTemp1 = new QLineEdit(groupBox);
        lineEdit_DensityOfHeatMediaPerTemp1->setObjectName(QStringLiteral("lineEdit_DensityOfHeatMediaPerTemp1"));
        lineEdit_DensityOfHeatMediaPerTemp1->setMinimumSize(QSize(61, 20));
        lineEdit_DensityOfHeatMediaPerTemp1->setMaximumSize(QSize(60, 20));

        gridLayout_2->addWidget(lineEdit_DensityOfHeatMediaPerTemp1, 1, 3, 1, 1);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_2->addItem(verticalSpacer, 0, 0, 1, 1);

        label_9 = new QLabel(groupBox);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setMinimumSize(QSize(20, 16));
        label_9->setMaximumSize(QSize(20, 16777215));

        gridLayout_2->addWidget(label_9, 2, 2, 1, 1);

        label_14 = new QLabel(groupBox);
        label_14->setObjectName(QStringLiteral("label_14"));
        label_14->setMinimumSize(QSize(40, 16));
        label_14->setMaximumSize(QSize(40, 16777215));

        gridLayout_2->addWidget(label_14, 4, 4, 1, 1);

        label_11 = new QLabel(groupBox);
        label_11->setObjectName(QStringLiteral("label_11"));
        label_11->setMinimumSize(QSize(20, 16));
        label_11->setMaximumSize(QSize(20, 16777215));

        gridLayout_2->addWidget(label_11, 3, 2, 1, 1);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer, 1, 5, 1, 1);


        gridLayout_13->addLayout(gridLayout_2, 2, 0, 1, 1);

        gridLayout_10 = new QGridLayout();
        gridLayout_10->setSpacing(6);
        gridLayout_10->setObjectName(QStringLiteral("gridLayout_10"));
        pushButton_Save = new QPushButton(groupBox);
        pushButton_Save->setObjectName(QStringLiteral("pushButton_Save"));
        pushButton_Save->setMinimumSize(QSize(100, 23));
        pushButton_Save->setMaximumSize(QSize(120, 23));

        gridLayout_10->addWidget(pushButton_Save, 0, 1, 1, 1);

        pushButton_Cancel = new QPushButton(groupBox);
        pushButton_Cancel->setObjectName(QStringLiteral("pushButton_Cancel"));
        pushButton_Cancel->setMinimumSize(QSize(100, 23));
        pushButton_Cancel->setMaximumSize(QSize(120, 23));

        gridLayout_10->addWidget(pushButton_Cancel, 0, 2, 1, 1);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_10->addItem(horizontalSpacer_2, 0, 0, 1, 1);


        gridLayout_13->addLayout(gridLayout_10, 3, 0, 1, 1);


        gridLayout_14->addWidget(groupBox, 0, 0, 1, 1);

        QWidget::setTabOrder(lineEdit_Calculated_Temp_Out_Air_Heating_Sys, lineEdit_Middle_Temp_Inner_Air_Heating_Flat);
        QWidget::setTabOrder(lineEdit_Middle_Temp_Inner_Air_Heating_Flat, lineEdit_Calculated_Temp_Out_Air_In_Transition_Period);
        QWidget::setTabOrder(lineEdit_Calculated_Temp_Out_Air_In_Transition_Period, lineEdit_lineEdit_Temp_HeatMedia_In_Flow_Pipe);
        QWidget::setTabOrder(lineEdit_lineEdit_Temp_HeatMedia_In_Flow_Pipe, lineEdit_Temp_HeatMedia_In_Return_Pipe);
        QWidget::setTabOrder(lineEdit_Temp_HeatMedia_In_Return_Pipe, lineEdit_Temp_HeatingMedia_In_Return_Pipe_For_Calculated_Temp_8);
        QWidget::setTabOrder(lineEdit_Temp_HeatingMedia_In_Return_Pipe_For_Calculated_Temp_8, lineEdit_Water_Heat_Capacity);
        QWidget::setTabOrder(lineEdit_Water_Heat_Capacity, lineEdit_Temp1);
        QWidget::setTabOrder(lineEdit_Temp1, lineEdit_DensityOfHeatMediaPerTemp1);
        QWidget::setTabOrder(lineEdit_DensityOfHeatMediaPerTemp1, lineEdit_Temp2);
        QWidget::setTabOrder(lineEdit_Temp2, lineEdit_DensityOfHeatMediaPerTemp2);
        QWidget::setTabOrder(lineEdit_DensityOfHeatMediaPerTemp2, lineEdit_Temp3);
        QWidget::setTabOrder(lineEdit_Temp3, lineEdit_DensityOfHeatMediaPerTemp3);
        QWidget::setTabOrder(lineEdit_DensityOfHeatMediaPerTemp3, lineEdit_Temp4);
        QWidget::setTabOrder(lineEdit_Temp4, lineEdit_DensityOfHeatMediaPerTemp4);
        QWidget::setTabOrder(lineEdit_DensityOfHeatMediaPerTemp4, pushButton_Cancel);
        QWidget::setTabOrder(pushButton_Cancel, pushButton_Save);
        QWidget::setTabOrder(pushButton_Save, groupBox);

        retranslateUi(SettingsDialog);

        QMetaObject::connectSlotsByName(SettingsDialog);
    } // setupUi

    void retranslateUi(QDialog *SettingsDialog)
    {
        SettingsDialog->setWindowTitle(QApplication::translate("SettingsDialog", "\320\230\320\267\320\274\320\265\320\275\320\265\320\275\320\270\320\265 \320\272\320\276\320\275\321\201\321\202\320\260\320\275\321\202", 0));
        groupBox->setTitle(QApplication::translate("SettingsDialog", "\320\232\320\276\320\275\321\201\321\202\320\260\320\275\321\202\321\213 \320\264\320\273\321\217 \321\200\320\260\321\201\321\207\320\265\321\202\320\276\320\262:", 0));
        label_3->setText(QApplication::translate("SettingsDialog", "\320\240\320\276\320\267\321\200\320\260\321\205. \320\277\320\260\321\200\320\260\320\274\320\265\321\202\321\200\320\270 \321\202\320\265\320\277\320\273\320\276\320\275\320\276\321\201\321\226\321\217 \320\262 \320\276\320\277\320\260\320\273\321\216\320\262. \320\277\320\265\321\200\321\226\320\276\320\264(\320\237\320\276\320\264\320\260\320\262\320\260\320\273\321\214\320\275\320\270\320\271 \321\202\321\200\321\203\320\261\320\276\320\277\321\200\320\276\320\262\321\226\320\264):", 0));
        label_4->setText(QApplication::translate("SettingsDialog", "\320\240\320\276\320\267\321\200\320\260\321\205. \320\277\320\260\321\200\320\260\320\274\320\265\321\202\321\200\320\270 \321\202\320\265\320\277\320\273\320\276\320\275\320\276\321\201\321\226\321\217 \320\262 \320\276\320\277\320\260\320\273\321\216\320\262. \320\277\320\265\321\200\321\226\320\276\320\264(\320\227\320\262\320\276\321\200\320\276\321\202\320\275\321\226\320\271 \320\264\321\200\321\203\320\261\320\276\320\277\321\200\320\276\320\262\321\226\320\264):", 0));
        label_2->setText(QApplication::translate("SettingsDialog", "\320\240\320\276\320\267\321\200\320\260\321\205. \321\202\320\265\320\274\320\277. \320\267\320\276\320\262\320\275. \320\277\320\276\320\262\321\226\321\202\321\200\321\217 \320\267\320\260 \321\203\320\274\320\276\320\262 \320\277\321\200\320\276\320\265\320\272\321\202. \321\201\320\270\321\201\321\202\320\265\320\274 \320\276\320\277\320\260\320\273\320\265\320\275\320\275\321\217:", 0));
        lineEdit_Calculated_Temp_Out_Air_Heating_Sys->setText(QString());
        label->setText(QApplication::translate("SettingsDialog", "\320\241\320\265\321\200\320\265\320\264\320\275\321\217 \321\202\320\265\320\274\320\277. \320\262\320\275\321\203\321\202\321\200\321\226\321\210. \320\277\320\276\320\262\321\226\321\202\321\200\321\217 \320\276\320\277\320\260\320\273\321\216\320\262\320\260\320\273\321\214\320\275\320\276\320\263\320\276 \320\277\321\200\320\270\320\274\321\226\321\211\320\265\320\275\320\275\321\217:", 0));
        lineEdit_Middle_Temp_Inner_Air_Heating_Flat->setText(QString());
        label_Cal->setText(QApplication::translate("SettingsDialog", "\320\240\320\276\320\267\321\200\320\260\321\205.\321\202\320\265\320\274\320\277. \320\267\320\276\320\262\320\275\321\226\321\210\320\275\321\214\320\276\320\263\320\276 \320\277\320\276\320\262\321\226\321\202\321\200\321\217 \320\262 \320\277\320\265\321\200\320\265\321\205\321\226\320\264\320\275\320\270\320\271 \320\277\320\265\321\200\321\226\320\276\320\264:", 0));
        lineEdit_Calculated_Temp_Out_Air_In_Transition_Period->setText(QString());
        label_Temp_Of_HeatingMedia_In_Flow_Pipe_In_Transition_Period_HeatinSys->setText(QApplication::translate("SettingsDialog", "\320\242\320\265\320\274\320\277. \321\202\320\265\320\277\320\273\320\276\320\275\320\276\321\201\321\226\321\217 \320\262 \320\277\320\276\320\264\320\260\320\262. \321\202\321\200\321\203\320\261\320\276\320\277\321\200\320\276\320\262\320\276\320\264\321\226 \320\262 \320\277\320\265\321\200\320\265\321\205\321\226\320\264. \320\277\320\265\321\200\321\226\320\276\320\264 \321\201\320\270\321\201\321\216 \320\276\320\277\320\260\320\273\320\265\320\275\320\275\321\217:", 0));
        lineEdit_lineEdit_Temp_HeatMedia_In_Flow_Pipe->setText(QString());
        label_6->setText(QApplication::translate("SettingsDialog", "\320\242\320\265\320\274\320\277. \321\202\320\265\320\277\320\273\320\276\320\275\320\276\321\201\321\226\321\217 \320\262 \320\267\320\262\320\276\321\200\320\276\321\202. \321\202\321\200\321\203\320\261\320\276\320\277\321\200\320\276\320\262\320\276\320\264\321\226 \320\262 \320\277\320\265\321\200\320\265\321\205\321\226\320\264. \320\277\320\265\321\200\321\226\320\276\320\264 \321\201\320\270\321\201\321\202. \320\276\320\277\320\260\320\273\320\265\320\275\320\275\321\217:", 0));
        lineEdit_Temp_HeatMedia_In_Return_Pipe->setText(QString());
        label_15->setText(QApplication::translate("SettingsDialog", "\320\242\320\265\320\274\320\277. \321\202\320\265\320\277\320\273\320\276\320\275\320\276\321\201\321\226\321\217 \320\262 \320\267\320\262. \321\202\321\200\321\203\320\261\320\276\320\277\321\200.\320\267\320\260 \321\200\320\276\320\267\321\200\320\260\321\205. \321\202\320\265\320\274\320\277. \320\267\320\276\320\262\320\275\321\226\321\210. \320\277\320\276\320\262\321\226\321\202\321\200\321\217 +8 \302\260C (\320\277\320\265\321\200\320\265\321\205\321\226\320\264. \320\277\320\265\321\200\321\226\320\276\320\264):", 0));
        lineEdit_Temp_HeatingMedia_In_Return_Pipe_For_Calculated_Temp_8->setText(QString());
        label_17->setText(QApplication::translate("SettingsDialog", "\320\242\320\265\320\277\320\273\320\276\321\224\320\274\320\275\321\226\321\201\321\202\321\214 \320\262\320\276\320\264\320\270:", 0));
        lineEdit_Water_Heat_Capacity->setText(QString());
        lineEdit_Temp4->setText(QApplication::translate("SettingsDialog", "95.00", 0));
        label_8->setText(QApplication::translate("SettingsDialog", "\320\272\320\263/\320\274\302\263", 0));
        label_13->setText(QApplication::translate("SettingsDialog", "\302\260C", 0));
        lineEdit_DensityOfHeatMediaPerTemp3->setText(QApplication::translate("SettingsDialog", "961.33", 0));
        label_20->setText(QApplication::translate("SettingsDialog", "\320\223\321\203\321\201\321\202\320\270\320\275\320\260 \321\202\320\265\320\277\320\273\320\276\320\275\320\276\321\201\321\226\321\217 \320\277\321\200\320\270 \321\202\320\265\320\274\320\277\320\265\321\200\320\260\321\202\321\203\321\200\321\226 t = ", 0));
        lineEdit_Temp3->setText(QApplication::translate("SettingsDialog", "95.00", 0));
        label_7->setText(QApplication::translate("SettingsDialog", "\302\260C", 0));
        lineEdit_DensityOfHeatMediaPerTemp2->setText(QApplication::translate("SettingsDialog", "961.33", 0));
        label_18->setText(QApplication::translate("SettingsDialog", "\320\223\321\203\321\201\321\202\320\270\320\275\320\260 \321\202\320\265\320\277\320\273\320\276\320\275\320\276\321\201\321\226\321\217 \320\277\321\200\320\270 \321\202\320\265\320\274\320\277\320\265\321\200\320\260\321\202\321\203\321\200\321\226 t = ", 0));
        lineEdit_Temp1->setText(QApplication::translate("SettingsDialog", "95.00", 0));
        label_21->setText(QApplication::translate("SettingsDialog", "\320\223\321\203\321\201\321\202\320\270\320\275\320\260 \321\202\320\265\320\277\320\273\320\276\320\275\320\276\321\201\321\226\321\217 \320\277\321\200\320\270 \321\202\320\265\320\274\320\277\320\265\321\200\320\260\321\202\321\203\321\200\321\226 t = ", 0));
        lineEdit_Temp2->setText(QApplication::translate("SettingsDialog", "95.00", 0));
        lineEdit_DensityOfHeatMediaPerTemp4->setText(QApplication::translate("SettingsDialog", "961.33", 0));
        label_10->setText(QApplication::translate("SettingsDialog", "\320\272\320\263/\320\274\302\263", 0));
        label_12->setText(QApplication::translate("SettingsDialog", "\320\272\320\263/\320\274\302\263", 0));
        label_19->setText(QApplication::translate("SettingsDialog", "\320\223\321\203\321\201\321\202\320\270\320\275\320\260 \321\202\320\265\320\277\320\273\320\276\320\275\320\276\321\201\321\226\321\217 \320\277\321\200\320\270 \321\202\320\265\320\274\320\277\320\265\321\200\320\260\321\202\321\203\321\200\321\226 t = ", 0));
        lineEdit_DensityOfHeatMediaPerTemp1->setText(QApplication::translate("SettingsDialog", "961.33", 0));
        label_9->setText(QApplication::translate("SettingsDialog", "\302\260C", 0));
        label_14->setText(QApplication::translate("SettingsDialog", "\320\272\320\263/\320\274\302\263", 0));
        label_11->setText(QApplication::translate("SettingsDialog", "\302\260C", 0));
        pushButton_Save->setText(QApplication::translate("SettingsDialog", "\320\241\320\276\321\205\321\200\320\260\320\275\320\270\321\202\321\214", 0));
        pushButton_Cancel->setText(QApplication::translate("SettingsDialog", "\320\236\321\202\320\274\320\265\320\275\320\260", 0));
    } // retranslateUi

};

namespace Ui {
    class SettingsDialog: public Ui_SettingsDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SETTINGSDIALOG_H
