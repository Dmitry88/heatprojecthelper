#include "settingsdialog.h"
#include "doublevalidator.h"
#include <QDebug>



SettingsDialog::SettingsDialog(QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);

	dataBase = QSqlDatabase::addDatabase("QSQLITE", "connection");
	dataBase.setDatabaseName("settings.db");
	dataBase.setUserName("admin");
	dataBase.setPassword("admin");

	

	if (dataBase.open())
	{
		qDebug() << "Database is open";

		QSqlQuery query(dataBase);

		bool bOk = query.exec("CREATE TABLE IF NOT EXISTS settings_constants("
			"id INTEGER PRIMARY KEY CHECK(id = 1),"  //1 only using for CHECK(id = 1) constrains 
			"isDefault INETEGER DEFAULT 0 CHECK(isDefault = 0 OR isDefault = 1), " //2
			"CalcTempOfMediaInHeatingPeriodFlowPipe REAL, " //3
			"CalcTempOfMediaInHeatingPeriodReturnPipe REAL, " //4
			"CalcTempOutAirPerCondDesignHeatSys REAL,"//5
			"MidTempInAirHeatingSpaceRoom REAL,"//6
			"CalcTempOutAirInTransitionPeriod REAL,"//7
			"TempOfMediaInFlowPipeInTransitionPeriodHeatingSys REAL,"//8
			"TempOfMediaInReturnPipeInTransitionPeriodHeatingSys REAL,"//9
			"TempOfMediaInReturnPipePerCalcTempOutAirTransitionPeriod REAL,"//10
			"WaterHeatCapacity REAL, " //11
			"Temp1 REAL, " //12
			"Temp2 REAL, " //13
			"Temp3 REAL, " //14
			"Temp4 REAL, " //15
			"DensityOfHeatMediaPerTemp1 REAL, "//16
			"DensityOfHeatMediaPerTemp2 REAL, "//17
			"DensityOfHeatMediaPerTemp3 REAL, "//18
			"DensityOfHeatMediaPerTemp4 REAL);"//19
			);


		query.exec("SELECT isDefault FROM settings_constants;");
		query.next();
		
		const int isDefault = query.value(0).toInt();

		if (0 == isDefault)
		{
			QString valTail2 = QString("%1, %2").arg(QString::number(17), QString::number(18));
			QString valTail = QString("%1, %2, %3, %4, %5, %6, %7, %8, %9").arg(
													   QString::number(9), 
													   QString::number(10),
													   QString::number(11),
													   QString::number(12),
													   QString::number(13),
													   QString::number(14),
													   QString::number(15),
													   QString::number(16),
													   valTail2);

			QString defaultVal = "INSERT INTO settings_constants ("
				"isDefault, "
				"CalcTempOfMediaInHeatingPeriodFlowPipe, "
				"CalcTempOfMediaInHeatingPeriodReturnPipe, "
				"CalcTempOutAirPerCondDesignHeatSys, "
				"MidTempInAirHeatingSpaceRoom, "
				"CalcTempOutAirInTransitionPeriod, "
				"TempOfMediaInFlowPipeInTransitionPeriodHeatingSys, "
				"TempOfMediaInReturnPipeInTransitionPeriodHeatingSys, "
				"TempOfMediaInReturnPipePerCalcTempOutAirTransitionPeriod, "
				"WaterHeatCapacity, "
				"Temp1, "
				"Temp2, "
				"Temp3, "
				"Temp4, "
				"DensityOfHeatMediaPerTemp1, "
				"DensityOfHeatMediaPerTemp2, "
				"DensityOfHeatMediaPerTemp3, "
				"DensityOfHeatMediaPerTemp4"
				") ";
				
			QString values = QString("VALUES(%1, %2, %3, %4, %5, %6, %7, %8, %9);").arg(
				QString::number(1),//isDefault
				QString::number(2.55),
				QString::number(3),
				QString::number(4),
				QString::number(5),
				QString::number(6),
				QString::number(7),
				QString::number(8), valTail);


			qDebug() << values; 

			defaultVal += values;
			qDebug() << defaultVal;

			bOk = query.exec(defaultVal);

			qDebug() << "CREATE Q: " << bOk;
			qDebug() << query.lastError().text();

		}

		qDebug() << "SELECT" <<
		query.exec("SELECT " 
			"CalcTempOfMediaInHeatingPeriodFlowPipe, "
			"CalcTempOfMediaInHeatingPeriodReturnPipe, "
			"CalcTempOutAirPerCondDesignHeatSys, "
			"MidTempInAirHeatingSpaceRoom, "
			"CalcTempOutAirInTransitionPeriod, "
			"TempOfMediaInFlowPipeInTransitionPeriodHeatingSys, "
			"TempOfMediaInReturnPipeInTransitionPeriodHeatingSys, "
			"TempOfMediaInReturnPipePerCalcTempOutAirTransitionPeriod, "
			"WaterHeatCapacity, "
			"Temp1, "
			"Temp2, "
			"Temp3, "
			"Temp4, "
			"DensityOfHeatMediaPerTemp1, "
			"DensityOfHeatMediaPerTemp2, "
			"DensityOfHeatMediaPerTemp3, "
			"DensityOfHeatMediaPerTemp4 "
			" FROM settings_constants;");

		query.next();


		double v1 = query.value("CalcTempOfMediaInHeatingPeriodFlowPipe").toFloat();
		double v2 = query.value("CalcTempOfMediaInHeatingPeriodReturnPipe").toFloat();
		double v3 = query.value("CalcTempOutAirPerCondDesignHeatSys").toFloat();
		double v4 = query.value("MidTempInAirHeatingSpaceRoom").toFloat();
		double v5 = query.value("CalcTempOutAirInTransitionPeriod").toFloat();
		double v6 = query.value("TempOfMediaInFlowPipeInTransitionPeriodHeatingSys").toFloat();
		double v7 = query.value("TempOfMediaInReturnPipeInTransitionPeriodHeatingSys").toFloat();
		double v8 = query.value("TempOfMediaInReturnPipePerCalcTempOutAirTransitionPeriod").toFloat();
		double v9 = query.value("WaterHeatCapacity").toFloat();
		double v10 = query.value("Temp1").toFloat();
		double v11 = query.value("Temp2").toFloat();
		double v12 = query.value("Temp3").toFloat();
		double v13 = query.value("Temp4").toFloat();
		double v14 = query.value("DensityOfHeatMediaPerTemp1").toFloat();
		double v15 = query.value("DensityOfHeatMediaPerTemp2").toFloat();
		double v16 = query.value("DensityOfHeatMediaPerTemp3").toFloat();
		double v17 = query.value("DensityOfHeatMediaPerTemp4").toFloat();
	


		ui.lineEdit_Calc_Temp_Of_Media_In_Heating_Period_Flow_Pipe->setText(QString::number(v1));
		ui.lineEdit_Calc_Temp_Of_Media_In_Heating_Period_Return_Pipe->setText(QString::number(v2));
		ui.lineEdit_Calculated_Temp_Out_Air_Heating_Sys->setText(QString::number(v3));
		ui.lineEdit_Middle_Temp_Inner_Air_Heating_Flat->setText(QString::number(v4));
		ui.lineEdit_Calculated_Temp_Out_Air_In_Transition_Period->setText(QString::number(v5));
		ui.lineEdit_lineEdit_Temp_HeatMedia_In_Flow_Pipe->setText(QString::number(v6));
		ui.lineEdit_Temp_HeatMedia_In_Return_Pipe->setText(QString::number(v7));
		ui.lineEdit_Temp_HeatingMedia_In_Return_Pipe_For_Calculated_Temp_8->setText(QString::number(v8));
		ui.lineEdit_Water_Heat_Capacity->setText(QString::number(v9));

		ui.lineEdit_Temp1->setText(QString::number(v10));
		ui.lineEdit_Temp2->setText(QString::number(v11));
		ui.lineEdit_Temp3->setText(QString::number(v12));
		ui.lineEdit_Temp4->setText(QString::number(v13));
		ui.lineEdit_DensityOfHeatMediaPerTemp1->setText(QString::number(v14));
		ui.lineEdit_DensityOfHeatMediaPerTemp2->setText(QString::number(v15));
		ui.lineEdit_DensityOfHeatMediaPerTemp3->setText(QString::number(v16));
		ui.lineEdit_DensityOfHeatMediaPerTemp4->setText(QString::number(v17));


	}

}

SettingsDialog::~SettingsDialog()
{

}

void SettingsDialog::on_pushButton_Save_clicked()
{
	QString  v1 = ui.lineEdit_Calc_Temp_Of_Media_In_Heating_Period_Flow_Pipe->text();
	QString  v2 = ui.lineEdit_Calc_Temp_Of_Media_In_Heating_Period_Return_Pipe->text();
	QString  v3 = ui.lineEdit_Calculated_Temp_Out_Air_Heating_Sys->text();
	QString  v4 = ui.lineEdit_Middle_Temp_Inner_Air_Heating_Flat->text();
	QString  v5 = ui.lineEdit_Calculated_Temp_Out_Air_In_Transition_Period->text();
	QString  v6 = ui.lineEdit_lineEdit_Temp_HeatMedia_In_Flow_Pipe->text();
	QString  v7 = ui.lineEdit_Temp_HeatMedia_In_Return_Pipe->text();
	QString  v8 = ui.lineEdit_Temp_HeatingMedia_In_Return_Pipe_For_Calculated_Temp_8->text();
	QString  v9 = ui.lineEdit_Water_Heat_Capacity->text();
	QString  v10 = ui.lineEdit_Temp1->text();
	QString  v11 = ui.lineEdit_Temp2->text();
	QString  v12 = ui.lineEdit_Temp3->text();
	QString  v13 = ui.lineEdit_Temp4->text();
	QString  v14 = ui.lineEdit_DensityOfHeatMediaPerTemp1->text();
	QString  v15 = ui.lineEdit_DensityOfHeatMediaPerTemp2->text();
	QString  v16 = ui.lineEdit_DensityOfHeatMediaPerTemp3->text();
	QString  v17 = ui.lineEdit_DensityOfHeatMediaPerTemp4->text();

	QSqlQuery query(dataBase);

	QString updateStr = QString ("UPDATE settings_constants SET  "
		"CalcTempOfMediaInHeatingPeriodFlowPipe = %1, "
		"CalcTempOfMediaInHeatingPeriodReturnPipe = %2, "
		"CalcTempOutAirPerCondDesignHeatSys = %3, "
		"MidTempInAirHeatingSpaceRoom = %4, "
		"CalcTempOutAirInTransitionPeriod = %5, "
		"TempOfMediaInFlowPipeInTransitionPeriodHeatingSys = %6, "
		"TempOfMediaInReturnPipeInTransitionPeriodHeatingSys = %7, "
		"TempOfMediaInReturnPipePerCalcTempOutAirTransitionPeriod = %8, "
		"WaterHeatCapacity = %9, "
		"Temp1 = %10, "
		"Temp2 = %11, "
		"Temp3 = %12, "
		"Temp4 = %13, "
		"DensityOfHeatMediaPerTemp1 = %14, "
		"DensityOfHeatMediaPerTemp2 = %15, "
		"DensityOfHeatMediaPerTemp3 = %16, "
		"DensityOfHeatMediaPerTemp4 = %17 "
		" ;").arg(v1, v2, v3, v4, v5, v6, v7, v8, v9).arg(v10, v11, v12, v13, v14, v15, v16, v17);

	qDebug() << "UPDATE: " << query.exec(updateStr);
	qDebug() << "UPDATE: " << query.lastError().text();

	SettingsDialog::close();
}

void SettingsDialog::on_pushButton_Cancel_clicked()
{
	SettingsDialog::close();
}


