#ifndef SETTINGSDIALOG_H
#define SETTINGSDIALOG_H

#include <QDialog>
#include "ui_settingsdialog.h"

#include <QSqlDatabase>
#include <QSqlTableModel>
#include <QSqlQuery>
#include <QSqlError>
#include <QTableView>

class SettingsDialog : public QDialog
{
	Q_OBJECT

public:
	SettingsDialog(QWidget *parent = 0);
	~SettingsDialog();


private slots:
	void on_pushButton_Save_clicked();
	void on_pushButton_Cancel_clicked();

private:
	void setValues();


private:
	Ui::SettingsDialog ui;
	QSqlDatabase dataBase;
};

#endif // SETTINGSDIALOG_H
