#include "word_editor.h"


bool WordtEditor::openDocument(QString filePath, bool visible)
{
	try
	{
		wordApplication = new QAxWidget("Word.Application");
		wordApplication->setProperty("Visible", true);
		documents = wordApplication->querySubObject("Documents");

		if (documents)
		{
			bool bb = documents->dynamicCall("Open(QVariant)", filePath.toStdString().c_str()).toBool();
		}
		else
		{
			return false;
		}

		if ((activeDocument = wordApplication->querySubObject("ActiveDocument")))
		{
			documentContent = activeDocument->querySubObject("Content");
		}
		else
		{
			return false;
		}
	}
	catch (...)
	{
		return false;
	}

	return true;
}

void WordtEditor::closeDocument()
{
	if (!wordApplication) return;

	bool bOk = wordApplication->dynamicCall("Quit()").toBool();

	if (bOk)
	{
		delete wordApplication;
		delete documents;
		delete activeDocument;
		delete documentContent;
	}
}


bool WordtEditor::saveAs(QString filePath)
{
	return false;
}

bool WordtEditor::replaceValueInText(QString key, QString value)
{
	QAxObject* content = activeDocument->querySubObject("Content");

	if (content)
	{
		QAxObject* find = content->querySubObject("Find");

		if (find)
		{
			value = QString::fromLocal8Bit(value.toLocal8Bit());

			QVariantList params; //= {
			//	QVariant(key), //FindText
			//	QVariant("False"),		  //MatchCase
			//	QVariant("False"),       //MatchWholeWord
			//	QVariant("True"),       //MatchWildcards
			//	QVariant("False"),       //MatchSoundsLike
			//	QVariant("False"),       //MatchAllWordForms
			//	QVariant("True"),       //Forward
			//	QVariant(1),            // maybe 1 int //Wrap
			//	QVariant("False"),       //Format
			//	QVariant(value),       //ReplaceWith
			//	QVariant(2),				//Replace
			//	QVariant("False"),       //MatchKashida
			//	QVariant("False"),       //MatchDiacritics
			//	QVariant("False"),       //MatchAlefHamza
			//	QVariant("False")      //MatchControl
			//};

			params.push_back(key);
			params.push_back("False");
			params.push_back("False");
			params.push_back("True");
			params.push_back("False");
			params.push_back("False");
			params.push_back("True");
			params.push_back(1);
			params.push_back("False");
			params.push_back(value);
			params.push_back(2);
			params.push_back("False");
			params.push_back("False");
			params.push_back("False");
			params.push_back("False");
			

			

			bool result =
				find->dynamicCall(
				"Execute(QVariant&, QVariant&, QVariant&, QVariant&, QVariant&, QVariant&, QVariant&, QVariant&, QVariant&, QVariant&, QVariant&, QVariant&, QVariant&, QVariant&, QVariant&)",
				params).toBool();

			return result;
		}
	}

	return false;
}

bool WordtEditor::replaceValueInFooter(QString key, QString value)
{
	QAxObject* sections = activeDocument->querySubObject("Sections(1)");

	if (!sections) return false;

	QAxObject* footer = sections->querySubObject("Footers(wdHeaderFooterPrimary)");

	if (!footer) return false;

	QAxObject* shapes = footer->querySubObject("Shapes");

	if (!shapes) return false;


	const int count = shapes->property("Count").toInt();

	for (int i = 0; i < count; i++)
	{
		QString str = QString("Item(%1)").arg(QString::number(i + 1));

		QAxObject* textBox = shapes->querySubObject(str.toStdString().c_str());

		if (!textBox) continue;

		QAxObject* textFrame = textBox->querySubObject("TextFrame");

		if (!textFrame) continue;

		QAxObject* textRange = textFrame->querySubObject("TextRange");

		if (!textRange) continue;

		QString text = textRange->property("Text").toString();


		if ((text == key) || (text == key + QString("\r")))
		{
			value = QString::fromLocal8Bit(value.toLocal8Bit());
			textRange->dynamicCall("SetText(QString)", value);
		}
	}

	return true;
}

bool WordtEditor::replaceValueInTextBox(QString key, QString value)
{
	QAxObject* shapes = activeDocument->querySubObject("Shapes");

	if (!shapes) return false;

	const int count = shapes->property("Count").toInt();


	for (int i = 0; i < count; i++)
	{
		QString str = QString("Item(%1)").arg(QString::number(i + 1));

		QAxObject* textBox = shapes->querySubObject(str.toStdString().c_str());

		if (!textBox) continue;

		QAxObject* textFrame = textBox->querySubObject("TextFrame");

		if (!textFrame) continue;

		QAxObject* textRange = textFrame->querySubObject("TextRange");

		if (!textRange) continue;

		QString text = textRange->property("Text").toString();

		if ((text == key) || (text == key + QString("\r")))
		{
			value = QString::fromLocal8Bit(value.toLocal8Bit());
			textRange->dynamicCall("SetText(QString)", value);
		}
	}

	return true;
}