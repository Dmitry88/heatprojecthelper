
#ifndef _CALCULATIONS_H_
#define _CALCULATIONS_H_

#include <math.h>

namespace Calculations {

const double waterHeatCapacity = 4.187;

const double densityOfHeatMedia1 = 961.92;
const double densityOfHeatMedia2 = 977.81;
const double densityOfHeatMedia3 = 991.27;
const double densityOfHeatMedia4 = 993.53;

const double t1 = 90.00;
const double t2 = 70.00;

const double t1_2 = 42.50;
const double t2_2 = 36.20;

double kWtToGCal(double kWt)
{
	return kWt*0.000858;
}

inline double Qomax(double q0, double A, double k1)
{
	return q0*A*(1 + k1)*0.001;
}

inline double Qomin(double Qomax, double innerTemp, double nTemp, double externalTemp)
{
	double d = innerTemp - externalTemp;

	if (0.0 != d)
	{
		double n = innerTemp - nTemp;
		return Qomax*(n/d);
	}

	return NAN;
}

inline double Qmomax(double Qomax)
{
	double d = waterHeatCapacity*(t1 - t2);

	if (0.0 != d)
	{
		return (3.6*Qomax * 1000000 * 1.163) / d;
	}

	return NAN;
}

inline double Qvomax(double Qmomax)
{
	return Qmomax / densityOfHeatMedia1;
}

inline double Qvomin(double Qmomax)
{
	return Qmomax / densityOfHeatMedia2;
}



inline double Qmonmax(double Qomin)
{
	double n = t1_2 - t2_2;

	if (n != 0)
	{
		n *= waterHeatCapacity;
		
		double d = 3.6*Qomin*1000000.0*1.163;

		return d / n;
	}

	return NAN;
}

inline double qvonmax(double Qmonmax)
{
	return Qmonmax / densityOfHeatMedia3;
}

inline double qvonmin(double Qmonmax)
{
	return Qmonmax / densityOfHeatMedia4;
}

}

#endif _CALCULATIONS_H_