#ifndef _COM_OBJECT_HELPER_H_
#define _COM_OBJECT_HELPER_H_

#include <QAxWidget>
#include <QAxObject.h>


class WordtEditor : public QObject {
	Q_OBJECT
public:
	WordtEditor() : wordApplication(nullptr), documents(nullptr),
						activeDocument(nullptr),documentContent(nullptr)
	{

	}

	~WordtEditor()
	{
		closeDocument();
	}

	WordtEditor(const WordtEditor&) = delete;
	WordtEditor& operator = (const WordtEditor&) = delete;

	bool openDocument(QString filePath, bool visible = false);
	void closeDocument();
	bool saveAs(QString filePath);

	bool replaceValueInText(QString key, QString value);
	bool replaceValueInFooter(QString key, QString value);
	bool replaceValueInTextBox(QString key, QString value);


private:
	/*void genereateDocumentation(const char* filePath, QAxObject* object)
	{
		QFile outfile(filePath);
		QTextStream out(&outfile);
		outfile.open(QIODevice::WriteOnly);
		QString docu = object->generateDocumentation();
		out << docu;
		outfile.close();
	}*/

private:
	QAxWidget* wordApplication;
	QAxObject* documents;
	QAxObject* activeDocument;
	QAxObject* documentContent;
};


#endif