#include "settings_sqlite_helper.h"
#include <QDebug.h>
#include <QSqlDatabase>
#include <QSqlTableModel>
#include <QSqlQuery>
#include <QSqlError>


SettingsSQLiteHelper::SettingsSQLiteHelper()
{

}

SettingsSQLiteHelper::~SettingsSQLiteHelper()
{

}

bool SettingsSQLiteHelper::open()
{
	dataBase = QSqlDatabase::addDatabase("QSQLITE", "connection");
	dataBase.setDatabaseName("settings.db");
	dataBase.setUserName("admin");
	dataBase.setPassword("admin");

	if (dataBase.open())
	{
		qDebug() << "Database is open";

		QSqlQuery query(dataBase);

		bool bOk = query.exec("CREATE TABLE IF NOT EXISTS settings_constants("
			"id INTEGER PRIMARY KEY CHECK(id = 1),"  //1 only using for CHECK(id = 1) constrains 
			"isDefault INETEGER DEFAULT 0 CHECK(isDefault = 0 OR isDefault = 1), " //2
			"CalcTempOfMediaInHeatingPeriodFlowPipe REAL, " //3
			"CalcTempOfMediaInHeatingPeriodReturnPipe REAL, " //4
			"CalcTempOutAirPerCondDesignHeatSys REAL,"//5
			"MidTempInAirHeatingSpaceRoom REAL,"//6
			"CalcTempOutAirInTransitionPeriod REAL,"//7
			"TempOfMediaInFlowPipeInTransitionPeriodHeatingSys REAL,"//8
			"TempOfMediaInReturnPipeInTransitionPeriodHeatingSys REAL,"//9
			"TempOfMediaInReturnPipePerCalcTempOutAirTransitionPeriod REAL,"//10
			"WaterHeatCapacity REAL, " //11
			"Temp1 REAL, " //12
			"Temp2 REAL, " //13
			"Temp3 REAL, " //14
			"Temp4 REAL, " //15
			"DensityOfHeatMediaPerTemp1 REAL, "//16
			"DensityOfHeatMediaPerTemp2 REAL, "//17
			"DensityOfHeatMediaPerTemp3 REAL, "//18
			"DensityOfHeatMediaPerTemp4 REAL);"//19
			);

		return bOk;
	}

	return false;
}

void SettingsSQLiteHelper::close()
{
	dataBase.close();
}