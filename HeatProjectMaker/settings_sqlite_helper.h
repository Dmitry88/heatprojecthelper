#ifndef _SETTINGS_SQLLITE_HELPER_H_
#define _SETTINGS_SQLLITE_HELPER_H_


#include <QSqlDatabase>


class SettingsSQLiteHelper {
public:
	SettingsSQLiteHelper();
	~SettingsSQLiteHelper();

	bool open();
	void close();

private:
	QSqlDatabase dataBase;

};


#endif //_SETTINGS_SQLLITE_HELPER_H_
